# -*- coding: utf-8 -*-
#Alunos: Lucas da Silva Nolasco, Matheus Bigarelli e Wagner R.U.A
#Documento como comando para criação e configuração do banco de dados da Rede Social.

import psycopg2
import psycopg2.extras 
import os

# Try to connect
try:
    conn = psycopg2.connect("dbname='1802LMWTechnologies' user='1802LMWTechnologies' host='200.134.10.32' password='971633'")
    print("Connected to the database.")
except:
    print("I am unable to connect to the database.") 

cur = conn.cursor() 

#Criando as tabelas
'''
cur.execute(""" CREATE TABLE Pessoa(
                Login VARCHAR(30) NOT NULL,
                Nome VARCHAR(80) NOT NULL,
                CidadeNatal VARCHAR(30) NOT NULL,
                PRIMARY KEY(Login)
                );""")

cur.execute(""" CREATE TABLE Conhece(
                Login1 VARCHAR(30) NOT NULL,
                Login2 VARCHAR(30) NOT NULL,
                PRIMARY KEY(Login1,Login2),
                FOREIGN KEY(Login1)
                    REFERENCES Pessoa(Login)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(Login2)
                    REFERENCES Pessoa(Login)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

cur.execute(""" CREATE TABLE Bloqueia(
                Login1 VARCHAR(30) NOT NULL,
                Login2 VARCHAR(30) NOT NULL,
                Razao VARCHAR(20) NOT NULL,
                PRIMARY KEY(Login1,Login2,Razao),
                FOREIGN KEY(Login1)
                    REFERENCES Pessoa(Login)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(Login2)
                    REFERENCES Pessoa(Login)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

cur.execute(""" CREATE TABLE Filme(
                ID VARCHAR(4) NOT NULL,
                Nome VARCHAR(30) NOT NULL,
                DataLancamento DATE NOT NULL,
                PRIMARY KEY(ID)
                );""")

cur.execute(""" CREATE TABLE CurteFilme(
                Login_Pessoa VARCHAR(30) NOT NULL,
                ID_Filme VARCHAR(4) NOT NULL,
                Nota INTEGER NOT NULL,
                PRIMARY KEY(Login_Pessoa,ID_Filme),
                FOREIGN KEY(Login_Pessoa)
                    REFERENCES Pessoa(Login)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(ID_Filme)
                    REFERENCES Filme(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

cur.execute(""" CREATE TABLE ArvoreCategorias(
                Categoria VARCHAR(20) NOT NULL,
                Superior VARCHAR(20),
                PRIMARY KEY(Categoria),
                FOREIGN KEY(Superior)
                    REFERENCES ArvoreCategorias(Categoria)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

cur.execute(""" CREATE TABLE Pertence(
                ID_Filme VARCHAR(4) NOT NULL,
                Categoria VARCHAR(20) NOT NULL,
                PRIMARY KEY(ID_Filme,Categoria),
                FOREIGN KEY(ID_Filme)
                    REFERENCES Filme(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(Categoria)
                    REFERENCES ArvoreCategorias(Categoria)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

cur.execute(""" CREATE TABLE Artista(
                ID VARCHAR(4) NOT NULL,
                Nome VARCHAR(30) NOT NULL,
                Telefone VARCHAR(15),
                Endereco VARCHAR(30)
                PRIMARY KEY(ID)
                );""")

cur.execute(""" CREATE TABLE Atua(
                ID_Filme VARCHAR(4) NOT NULL,
                ID_Artista VARCHAR(4) NOT NULL,
                PRIMARY KEY(ID_Filme,ID_Artista),
                FOREIGN KEY(ID_Filme)
                    REFERENCES Filme(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(ID_Artista)
                    REFERENCES Artista(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION 
                );""")

cur.execute(""" CREATE TABLE Dirige(
                ID_Filme VARCHAR(4) NOT NULL,
                ID_Artista VARCHAR(4) NOT NULL,
                PRIMARY KEY(ID_Filme),
                FOREIGN KEY(ID_Filme)
                    REFERENCES Filme(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(ID_Artista)
                    REFERENCES Artista(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION 
                );""")

cur.execute(""" CREATE TABLE ArtistaMusical(
                ID VARCHAR(4) NOT NULL,
                NomeArtistico VARCHAR(20) NOT NULL,
                Genero VARCHAR(15) NOT NULL,
                Pais VARCHAR(20) NOT NULL,
                PRIMARY KEY(ID)
                );""")

cur.execute(""" CREATE TABLE CurteArtistaMusical(
                Login_Pessoa VARCHAR(30) NOT NULL,
                ID_ArtistaMusical VARCHAR(4) NOT NULL,
                Nota INTEGER NOT NULL,
                PRIMARY KEY(Login_Pessoa,ID_ArtistaMusical),
                FOREIGN KEY(Login_Pessoa)
                    REFERENCES Pessoa(Login)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(ID_ArtistaMusical)
                    REFERENCES ArtistaMusical(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

cur.execute(""" CREATE TABLE Musico(
                ID VARCHAR(4) NOT NULL,
                NomeReal VARCHAR(30) NOT NULL,
                EstiloMusical VARCHAR(20) NOT NULL,
                DataNascimento DATE NOT NULL,
                PRIMARY KEY(ID)
                );""")

cur.execute(""" CREATE TABLE Cantor(
                ID VARCHAR(4) NOT NULL,
                ID_Musico VARCHAR(30) NOT NULL,
                PRIMARY KEY(ID),
                FOREIGN KEY(ID)
                    REFERENCES ArtistaMusical(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(ID_Musico)
                    REFERENCES Musico(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

cur.execute(""" CREATE TABLE Banda(
                ID VARCHAR(4) NOT NULL,
                PRIMARY KEY(ID),
                FOREIGN KEY(ID)
                    REFERENCES ArtistaMusical(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

cur.execute(""" CREATE TABLE Possui(
                ID VARCHAR(4) NOT NULL,
                ID_Musico VARCHAR(30) NOT NULL,
                PRIMARY KEY(ID,ID_Musico),
                FOREIGN KEY(ID)
                    REFERENCES ArtistaMusical(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION,
                FOREIGN KEY(ID_Musico)
                    REFERENCES Musico(ID)
                        ON DELETE NO ACTION
                        ON UPDATE NO ACTION
                );""")

#Adicionando Categorias
cur.execute("""INSERT INTO ArvoreCategorias VALUES('Ação',NULL)""")
cur.execute("""INSERT INTO ArvoreCategorias VALUES('Aventura')""")
cur.execute("""INSERT INTO ArvoreCategorias VALUES('Ficção Científica')""")
cur.execute("""INSERT INTO ArvoreCategorias VALUES('Super-Heróis','Ficção Científica')""")


#Adicionando filmes de exemplo.
cur.execute("""INSERT INTO Filme VALUES('1','Iron Man','2008-04-14')""")

cur.execute("""INSERT INTO Pertence VALUES('1','Ação')""")
cur.execute("""INSERT INTO Pertence VALUES('1','Aventura')""")
cur.execute("""INSERT INTO Pertence VALUES('1','Super-Heróis')""")
'''

for i in range (13,59):
	cur.execute("DELETE FROM ArtistaMusical WHERE id='" + str(i) + "'") 

conn.commit()
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
