#!/usr/bin/python
#-*- coding: utf-8 -*-

import sys
from dataBaseController import DataBaseController
import matplotlib.pyplot as plt

def menu():

    print "\n" + "-"*20 + "Questões disponíveis:" + "-"*20 
    print "1. Qual é a média e desvio padrão dos ratings para artistas musicais e filmes?"
    print "2. Quais são os artistas e filmes com o maior rating médio curtidos por pelo menos duas pessoas?"
    print "3. Quais são os 10 artistas musicais e filmes mais populares?"
    #4. Crie uma view chamada ConheceNormalizada que represente simetricamente os relacionamentos de conhecidos da turma. Por exemplo, se a conhece b mas b não declarou conhecer a, a view criada deve conter o relacionamento (b,a) além de (a,b).
    ''' Resolucao da criacao de VIEW
    CREATE VIEW ConheceNormalizada AS
    (SELECT login1, login2 FROM Conhece
     UNION
     SELECT login2, login1 FROM Conhece);
    '''
	
    print "5. Quais são os conhecidos que compartilham o maior numero de filmes curtidos?"
    print "6. Qual o número de conhecidos dos conhecidos para cada integrante do grupo?"
    print "7. Gráfico para a função f(x) = (número de pessoas que curtiram exatamente x filmes)"
    print "8. Gráfico para a função f(x) = (número de filmes curtidos por exatamente x pessoas)"
    print "9. Filmes recomendados para um usuário." 
    print "10. Pessoas alcançáveis por um usuário com um determinado número de passos."

def mediaEDesvioPadrao(database, opcao):
    data = []

    # Filme
    if opcao == 2:
	data = database.selectInTable('''SELECT id_filme, CAST(AVG(nota) AS float), CAST(STDDEV(nota) AS float) FROM CurteFilme GROUP BY id_filme;''')

    else:
	data = database.selectInTable(''' SELECT id_artistamusical, CAST(AVG(nota) AS float), CAST(STDDEV(Nota) AS float) FROM CurteArtistaMusical GROUP BY id_artistamusical;''')
    return data

def maioresRatingMedio(database, tabela,  quantidade = None):
    # 1 - Artista Musical
    # 2 - Filmes

    data = []
    if tabela == 1:
        data = database.selectInTable('''SELECT A.NomeArtistico, CAST(AVG(CAM.Nota) AS float) AS media 
                                            FROM ArtistaMusical A JOIN CurteArtistaMusical CAM
                                                ON A.ID = CAM.Id_ArtistaMusical
                                            GROUP BY A.NomeArtistico
                                            HAVING COUNT(*) >= 2
                                            ORDER BY media DESC; ''')

    elif tabela == 2:
        data = database.selectInTable('''SELECT F.Id, CAST(AVG(CF.Nota) AS float) AS media 
                                            FROM Filme F JOIN CurteFilme CF
                                                ON F.ID = CF.Id
                                            GROUP BY F.Id
                                            HAVING COUNT(*) >= 2
                                            ORDER BY media DESC; ''')  # Mudar pra pesquisar filmes

    if quantidade != None and quantidade < len(data):
        data = data[:quantidade]

    return data

def getPopulares(database, tabela):
    # 1 - Artista Musical
    # 2 - Filmes

    data = []
    if tabela == 1:
        data = database.selectInTable('''SELECT A.NomeArtistico, COUNT(*) AS likes
                                            FROM ArtistaMusical A JOIN CurteArtistaMusical CAM 
                                                ON A.ID = CAM.Id_ArtistaMusical
                                            GROUP BY A.NomeArtistico
                                            ORDER BY likes DESC
                                            LIMIT 10; ''')

    elif tabela == 2:
        data = database.selectInTable('''SELECT F.Id, COUNT(*) AS likes
                                            FROM Filme F JOIN CurteFIlme CF 
                                                ON F.ID = CF.Id_filme
                                            GROUP BY F.Id
                                            ORDER BY likes DESC
                                            LIMIT 10; ''')    # Mudar pra pesquisar filmes

    return data

def pessoasFilmesComum(database):
    data = database.selectInTable('''WITH FilmesComum AS
                                    (SELECT CF1.login_pessoa AS login1, CF2.login_pessoa AS login2, CAST(COUNT(*) AS int) AS comum
                                        FROM CurteFilme CF1, CurteFilme CF2
                                        WHERE CF1.login_pessoa > CF2.login_pessoa AND CF1.id_filme = CF2.id_filme
                                        GROUP BY CF1.login_pessoa, CF2.login_pessoa)

                                    SELECT login1, login2, comum FROM FilmesComum
                                        WHERE comum = (SELECT MAX(comum) FROM FilmesComum); ''')
    return data[0]

def numeroConhecidosConhecidos(database):
    data = database.selectInTable('''WITH Conhecidos AS
                                    (SELECT login1, login2
                                        FROM ConheceNormalizada
                                        WHERE login1 = 'DI1802lucasnolasco'
                                            OR login1 = 'DI1802matheuscosta'
                                            OR login1 = 'DI1802wagneragostinho')

                                    SELECT Conhecidos.login1, COUNT(*)
                                        FROM ConheceNormalizada, Conhecidos
                                        WHERE ConheceNormalizada.login1 = Conhecidos.login2
                                            AND ConheceNormalizada.login2 != Conhecidos.login1
                                        GROUP BY Conhecidos.login1; ''')

    return data

def numPessoasxFilmes(database):
    data = database.selectInTable('''WITH FilmesPessoa AS
                                    (SELECT login_pessoa AS login1, COUNT(*) AS num_filmes
                                        FROM CurteFilme
                                        GROUP BY login_pessoa)

                                    SELECT CAST(num_filmes AS int), CAST(COUNT(*) AS int)
                                        FROM FilmesPessoa
                                        GROUP BY num_filmes
                                        ORDER BY num_filmes;''')

    x = []
    y = []
    for linha in data:
        x.append(linha[0])
        y.append(linha[1])

    return x, y

def numFilmesxPessoas(database):
    data = database.selectInTable('''WITH FilmesPessoa AS
                                    (SELECT id_filme AS id, COUNT(*) AS num_pessoas
                                        FROM CurteFilme
                                        GROUP BY id_filme)

                                    SELECT CAST(num_pessoas AS int), CAST(COUNT(*) AS int)
                                        FROM FilmesPessoa
                                        GROUP BY num_pessoas
                                        ORDER BY num_pessoas;''')

    x = []
    y = []
    for linha in data:
        x.append(linha[0])
        y.append(linha[1])

    return x, y

def recomendacaoDeFilmes(database, _usuario):  
    _usuario = '\'' + _usuario + '\''
    data = database.selectInTable('''	WITH FilmesComum AS
					(SELECT CN.login2, COUNT(*) AS num_filmes
					FROM ConheceNormalizada CN, CurteFilme  CF1, CurteFilme CF2
					WHERE CN.login1 =  ''' + _usuario + '''
					        AND CF1.id_filme = CF2.id_filme
						AND CF1.login_pessoa = CN.login1
						AND CF2.login_pessoa = CN.login2
					GROUP BY CN.login2)

					SELECT Filme.id
					FROM Filme, CurteFilme CF
					WHERE CF.login_pessoa = (SELECT login2 FROM FilmesComum WHERE num_filmes = (SELECT MAX(num_filmes) FROM FilmesComum))
						AND Filme.id = CF.id_filme
						AND CF.id_filme NOT IN(SELECT id_filme FROM CurteFilme WHERE login_pessoa = ''' + _usuario + ''')''')
    x = []
    for linha in data:
	x.append(linha[0])
    return x

def numeroDePassos(database, usuario, n_passos):
    usuario = '\'' + usuario + '\''
    data = database.selectInTable( '''  WITH RECURSIVE Conhece AS
					(
					SELECT login2, 0 nivel
					FROM ConheceNormalizada
					WHERE login1 = ''' + usuario + '''

					UNION ALL

					SELECT DISTINCT CN.login2, Conhece.nivel + 1
					FROM Conhece, ConheceNormalizada CN
					WHERE Conhece.login2 = CN.login1
						AND CN.login2 != ''' + usuario + '''
						AND Conhece.nivel < ''' + n_passos + '''
				        )

					SELECT login2, MIN(nivel) FROM Conhece 
					GROUP BY login2
					ORDER BY MIN(nivel);''')
    
    return data

def main():
    database = DataBaseController("1802LMWTechnologies", "1802LMWTechnologies", "200.134.10.32", "971633")

    opcao = 1
    while opcao >= 1 and opcao <= 10:
	menu()
	opcao = int(raw_input('Escolha uma opcao: '))
	executaOpcao(opcao, database)

def executaOpcao(opcao, database):
    if opcao == 1:
	infos_rating_music = mediaEDesvioPadrao(database, 1)
	infos_rating_movie = mediaEDesvioPadrao(database, 2)

	for info in infos_rating_music:
	    if not info[2]:
		info[2] = 0
	    print "Artista: " + "{:60s}".format(info[0]) + "\t\tMedia: " + "{:2.2f}".format(info[1]) + "  Desvio padrao: " + "{:.2f}".format(info[2])

	for info in infos_rating_movie:
	    if not info[2]:
		info[2] = 0
	    print "Filme: " + "{:60s}".format(info[0]) + "\t\tMedia: " + "{:2.2f}".format(info[1]) + "  Desvio padrao: " + "{:.2f}".format(info[2])

    if opcao == 2:
        top_rating_art_music = maioresRatingMedio(database, 1)
        top_rating_filmes = maioresRatingMedio(database, 2)

        for art_music in top_rating_art_music:
            print "Artista: ", "{:22s}".format(art_music[0]), "\t\t\tNota: ", "{:.2f}".format(art_music[1])

        for filme in top_rating_filmes:
            print "Filme: ", "{:30s}".format(filme[0]), "\t\t\tNota: ","{:.2f}".format(filme[1])

    elif opcao == 3:
        most_popular_art_music = getPopulares(database, 1)
        most_popular_filmes = getPopulares(database, 2)

        for art_music in most_popular_art_music:
            print "Artista: ", "{:30s}".format(art_music[0]), " Likes: ", art_music[1]

        for filme in most_popular_filmes:
            print "Filme:   ", "{:30s}".format(filme[0]), " Likes: ", filme[1]

    elif opcao == 4:
        pass    # Conferir se precisa mesmo disso

    elif opcao == 5:
        pessoas_filmes_comum = pessoasFilmesComum(database)
        print "Pessoas: ", pessoas_filmes_comum[0], " e ", pessoas_filmes_comum[1], " Filmes em comum: ", pessoas_filmes_comum[2]

    elif opcao == 6:
        num_conhecidos_conhecidos = numeroConhecidosConhecidos(database)
        for pessoa in num_conhecidos_conhecidos:
            print "Membro do Grupo: ", pessoa[0], " Numero de Conhecidos dos Conhecidos: ", pessoa[1]

    elif opcao == 7:
        x, y = numPessoasxFilmes(database)
        plt.plot(x, y, 'ro')
        plt.ylabel("Numero de Pessoas")
        plt.xlabel("Filmes Curtidos")
        plt.title("Pessoas x Quantidade de Filmes")
        plt.show()

    elif opcao == 8:
        x, y = numFilmesxPessoas(database)
        plt.plot(x, y, 'ro')
        plt.xlabel("Numero de Pessoas")
        plt.ylabel("Filmes Curtidos")
        plt.title("Pessoas x Quantidade de Filmes")
        plt.show()

    elif opcao == 9:
	usuario = raw_input('Informe um login: ')
	recomendacoes = recomendacaoDeFilmes(database,usuario)
	for recomendacao in recomendacoes:
	    print "Filme: " + recomendacao 
    
    elif opcao == 10:
	usuario = raw_input('Informe um Login: ')
	n_passos = raw_input('Informe um numéro de passos: ')
	data = numeroDePassos(database,usuario,n_passos)
	for linha in data:
	    print "Pessoa: " + '{:30s}'.format(linha[0]) + " Numero de Passos: " + str(linha[1])

if __name__ == "__main__":
    main()
