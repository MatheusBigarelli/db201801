from GrafosApp.dataBaseController import DataBaseController
from django.shortcuts import render


# Create your views here.
def homePage(request):
    return render(request, 'GrafosApp/home.html')

def multilayerPage(request):
    return render(request, 'GrafosApp/multilayer.html')

def stepsPage(request):
    if request.POST:
        steps = request.POST.get('steps')
        destiny = request.POST.get('destiny')
        table = getGraphOfSteps(steps, destiny)
        return render(request, 'GrafosApp/findsteps.html', {'table' : table, 'steps' : steps})

    return render(request, 'GrafosApp/findsteps.html')

def getGraphOfSteps(steps, destiny):
    database = DataBaseController("1802LMWTechnologies", "1802LMWTechnologies", "200.134.10.32", "971633")
    destiny = '\'' + destiny + '\''

    consult = """WITH RECURSIVE Conhece AS(
                    SELECT login2, 1 nivel
                    FROM ConheceNormalizada
                    WHERE login1 = """ + destiny + """

                    UNION ALL

                    SELECT DISTINCT CN.login2, Conhece.nivel + 1
                    FROM Conhece, ConheceNormalizada CN
                    WHERE Conhece.login2 = CN.login1
                        AND CN.login2 != """ + destiny + """
                        AND Conhece.nivel < """ + str(steps) + """
                )

                SELECT """ + destiny + """ AS destiny, login2, MIN(nivel) FROM Conhece 
                GROUP BY login2
                ORDER BY MIN(nivel);"""

    table = database.selectInTable(consult)
    return table
