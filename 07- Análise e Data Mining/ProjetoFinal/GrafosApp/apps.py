from django.apps import AppConfig


class GrafosappConfig(AppConfig):
    name = 'GrafosApp'
