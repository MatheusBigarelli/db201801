//Para deixar selecionar somente uma checkbox
$('input[type="checkbox"]').on('change', function() {
    $('input[type="checkbox"]').not(this).prop('checked', false);
 });

function strToObj(str){
    var obj = {};
    if(str||typeof str ==='string'){
        var objStr = str.match(/\{(.)+\}/g);
        eval("obj ="+objStr);
    }
    return obj
}

function chooseGraph(){
    var checkbox = [];
    for(i=1; i < 8; i++){
        checkbox[i] = document.getElementById("Checkbox" + i.toString() ).checked;
    }

    var json_path;
    if(checkbox[1])
        json_path = json_path_connectivity_by_people;
    else if(checkbox[2])
        json_path = json_path_connectivity_by_artists;
    else if(checkbox[3])
        json_path = json_path_connectivity_by_movies;
    else if(checkbox[4])
        json_path = json_path_similarity_people_artists;
    else if(checkbox[5])
        json_path = json_path_similarity_people_movies;
    else if(checkbox[6])
        json_path = json_path_similarity_artists_movies;
    else if(checkbox[7])
        json_path = json_path_similarity;
    else
        return;

    //Limpando a tela
    var g = document.getElementById('graph-container');
    var p = g.parentNode;
    p.removeChild(g);
    var c = document.createElement('div');
    c.setAttribute('id', 'graph-container');
    p.appendChild(c);

    loadGraph(json_path)
}

sigma.classes.graph.addMethod('neighbors', function(nodeId) {
    var k,
        neighbors = {},
        index = this.allNeighborsIndex[nodeId] || {};

    for (k in index)
        neighbors[k] = this.nodesIndex[k];

    return neighbors;
});

function loadGraph(json_path){
    sigma.parsers.json(json_path, {
            container: 'graph-container',
            renderer: {
                container: document.getElementById('graph-container'),
                type: 'canvas'
            },
            settings: {
                minNodeSize: 4,
                maxNodeSize: 15,
                minEdgeSize: 0.3,
                maxEdgeSize: 1,
                labelThreshold: '15',
                labelSize: "proportional",
                labelSizeRatio: 1,
                doubleClickEnabled: false,
                hideEdgesOnMove: true
            }
    }, 
    function(s) 
    {
        function showNodeAttributes(n){
            var body = document.getElementById("body");
            var div_modal_dialog = document.createElement("div");
            div_modal_dialog.className = "modal-dialog text-center modal2";
            div_modal_dialog.id = "modal2";
            var div = document.createElement("div");
            div.className = "main-section";
            var div_modal_content = document.createElement("div");
            div_modal_content.className = "modal-content content2";
            div_modal_content.id = "modal-content";
            body.appendChild(div_modal_dialog);
            div_modal_dialog.appendChild(div);
            div.appendChild(div_modal_content);
            
            var p = [];

            p[0] = document.createElement("strong");
            p[0].className = "node-atrributes";
            p[0].innerHTML = n.label;

            p[1] = document.createElement("p");
            p[1].className = "node-atrributes";
            p[1].innerHTML = n.id;

            for(j in p){
                document.getElementById("modal-content").appendChild(p[j]);
            }
        }                                                                                                       

        function deleteNodeAtrributes(){
            var body = document.getElementById("body");
            var div = document.getElementById("modal2");
            body.removeChild(div);
        }
        //Funcao para resetar a cor de todos os nos e arestas.
        function refreshGraph(){
            s.graph.nodes().forEach(function(n) {
                console.log(n)
                n.color = n.originalColor;
                n.label = n.originalLabel;
            });

            s.graph.edges().forEach(function(e) {
                e.color = e.originalColor;
            });

            s.refresh();
        }

        //Mapeamos o grafo para ser usado no algoritimo de djisktra.
        var graph_mapping = '{'
        s.graph.nodes().forEach(function(n) {
            //Guardamos a cor e o label original de cada no. 
            n.originalColor = n.color;
            n.originalLabel = n.label;

            graph_mapping += n.id + ':{'
            s.graph.edges().forEach(function(e) {
                if(e.source == n.id)
                graph_mapping += e.target + ':1,';
            });
            if(graph_mapping[graph_mapping.length -1] == ','){
                graph_mapping = graph_mapping.slice(0,-1);
                graph_mapping += '},';
            }
            else
                graph_mapping += '},';
        });
        graph_mapping = graph_mapping.slice(0,-1);
        graph_mapping += '}';

        var graph_for_dijkstra = new Graph(strToObj(graph_mapping));
        
        //Variaveis para os botões do html
        var btn_path = document.getElementById("btn-path");
        var btn_refresh = document.getElementById("btn-refresh");

        //Adiciona funcao de poder arrastar um no.
        var dragListener = sigma.plugins.dragNodes(s, s.renderers[0]);
        
        //Guardamos a cor original de todas as arestas e removemos nós duplicados
        s.graph.edges().forEach(function(e1) {
            e1.originalColor = e1.color;
            s.graph.edges().forEach(function(e2) {
                if(e1.target == e2.source && e2.target == e1.source && e1.id > e2.id){
                    s.graph.dropEdge(e2.id);
                    e1.type = 'curve';
                }
            });
        });

        s.refresh();

        //Se um nó for clicado mostramos somente os seus vizinhos.
        s.bind('clickNode', function(e) {
            var nodeId = e.data.node.id,
                toKeep = s.graph.neighbors(nodeId);
                toKeep[nodeId] = e.data.node;

            s.graph.nodes().forEach(function(n) {
                if (toKeep[n.id]){
                    n.color = n.originalColor;
                    n.label = n.originalLabel;
                }
                else{
                    n.label = false;
                    n.color = '#eee';
                }
            });

            s.graph.edges().forEach(function(e) {
                if (toKeep[e.source] && toKeep[e.target])
                    e.color = e.originalColor;
                else
                    e.color = '#eee';
            });

            s.refresh();

            try {
                deleteNodeAtrributes();
                showNodeAttributes(e.data.node);
            }
            catch{
                showNodeAttributes(e.data.node);
            }
        });

        //Se o botão de refresh for clicado...
        btn_refresh.addEventListener('click', function(e) {
            refreshGraph()
            deleteNodeAtrributes();
        });

        //Achar o caminho com distancia euclidiana
        

    });
};

document.getElementById("Checkbox1").checked = true;
chooseGraph();

var btn_filter = document.getElementById("btn-filter");
btn_filter.addEventListener('click', function(e) {
    chooseGraph();
});

