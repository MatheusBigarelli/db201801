function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}

s = new sigma({
    container: 'graph-container',
    renderer: {
        container: document.getElementById('graph-container'),
        type: 'canvas'
    },
    settings: {
        minNodeSize: 4,
        maxNodeSize: 15,
        minEdgeSize: 0.5,
        maxEdgeSize: 1,
        labelThreshold: '16',
        labelSize: "proportional",
        labelSizeRatio: 1,
        doubleClickEnabled: false,
        hideEdgesOnMove: true,
        animationsTime: 1000
    }
});

var center_node;
var nodes_per_distance = []
var colors = ['#33ccff', '#db4dff','#33ccff', '#db4dff','#33ccff', '#db4dff']
for(i=0; i <= steps; i++)
    nodes_per_distance[i] = 0;
nodes_per_distance[0] = 1

for(line in graph_info){
    if(line== 0){
        center_node = graph_info[line][0];
        s.graph.addNode({
            // Main attributes:
            id: center_node,
            label: center_node,
            // Display attributes:
            x: 0,
            y: 0,
            size: 1,
            color: '#33ccff',
            attributes: {distance: 0}
        });
    }

    s.graph.addNode({
        // Main attributes:
        id: graph_info[line][1],
        label: graph_info[line][1],
        // Display attributes:
        x: 1,
        y: 1,
        size: 1,
        color: colors[graph_info[line][2]],
        attributes: {distance: graph_info[line][2]}
      }).addEdge({
        id: 'e' + line.toString() ,
        // Reference extremities:
        source: center_node,
        target: graph_info[line][1],
        size: (graph_info[line][2]),
        color: '#33ccff'
      });

    nodes_per_distance[ graph_info[line][2] ]++;
}

s.graph.nodes().forEach(function(node, i, a) {
    node.x = Math.cos(Math.PI * 2 * i / ((nodes_per_distance[node.attributes.distance])) )*node.attributes.distance;
    node.y = Math.sin(Math.PI * 2 * i / ((nodes_per_distance[node.attributes.distance])) )*node.attributes.distance;
    s.refresh();
});

function showNodeAttributes(n){
    var body = document.getElementById("body");
    var div_modal_dialog = document.createElement("div");
    div_modal_dialog.className = "modal-dialog text-center modal2";
    div_modal_dialog.id = "modal2";
    var div = document.createElement("div");
    div.className = "main-section";
    var div_modal_content = document.createElement("div");
    div_modal_content.className = "modal-content content2";
    div_modal_content.id = "modal-content";
    body.appendChild(div_modal_dialog);
    div_modal_dialog.appendChild(div);
    div.appendChild(div_modal_content);
    
    var p = [];

    p[0] = document.createElement("p");
    p[0].className = "node-atrributes";
    p[0].innerHTML = n.label;


    for(j in p){
        document.getElementById("modal-content").appendChild(p[j]);
    }
}                                                                                                       

function deleteNodeAtrributes(){
    var body = document.getElementById("body");
    var div = document.getElementById("modal2");
    body.removeChild(div);
}

//Se um nó for clicado mostramos somente os seus vizinhos.
s.bind('clickNode', function(e) {
    try {
        deleteNodeAtrributes();
        showNodeAttributes(e.data.node);
    }
    catch{
        showNodeAttributes(e.data.node);
    }
});