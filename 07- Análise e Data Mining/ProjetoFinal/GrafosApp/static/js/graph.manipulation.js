//Para deixar selecionar somente uma checkbox
$('input[type="checkbox"]').on('change', function() {
    $('input[type="checkbox"]').not(this).prop('checked', false);
 });

function strToObj(str){
    var obj = {};
    if(str||typeof str ==='string'){
        var objStr = str.match(/\{(.)+\}/g);
        eval("obj ="+objStr);
    }
    return obj
}

sigma.classes.graph.addMethod('neighbors', function(nodeId) {
    var k,
        neighbors = {},
        index = this.allNeighborsIndex[nodeId] || {};

    for (k in index)
        neighbors[k] = this.nodesIndex[k];

    return neighbors;
});


sigma.parsers.json(json_path_conhecidos, {
        container: 'graph-container',
        renderer: {
            container: document.getElementById('graph-container'),
            type: 'canvas',
        },
        settings: {
            minNodeSize: 4,
            maxNodeSize: 15,
            maxEdgeSize: 0.5,
            labelThreshold: '16',
            labelSize: "proportional",
            labelSizeRatio: 0.8,
            doubleClickEnabled: false,
            hideEdgesOnMove: true
        }
}, 
function(s) 
{
    var nodes_attributes = s.graph.nodes()[0].attributes;
    var option;
    for(attribute in nodes_attributes){
        option = document.createElement("option");
        option.innerHTML = attribute;
        document.getElementById("select-option").appendChild(option);
    }

    function showNodeAttributes(n){
        var body = document.getElementById("body");
        var div_modal_dialog = document.createElement("div");
        div_modal_dialog.className = "modal-dialog text-center modal2";
        div_modal_dialog.id = "modal2";
        var div = document.createElement("div");
        div.className = "main-section";
        var div_modal_content = document.createElement("div");
        div_modal_content.className = "modal-content content2";
        div_modal_content.id = "modal-content";
        body.appendChild(div_modal_dialog);
        div_modal_dialog.appendChild(div);
        div.appendChild(div_modal_content);
        
        var p = [];

        p[0] = document.createElement("strong");
        p[0].className = "node-atrributes";
        p[0].innerHTML = n.label;

        p[1] = document.createElement("p");
        p[1].className = "node-atrributes";
        p[1].innerHTML = n.id;

        for(j in p){
            document.getElementById("modal-content").appendChild(p[j]);
        }
    }                                                                                                       

    function deleteNodeAtrributes(){
        var body = document.getElementById("body");
        var div = document.getElementById("modal2");
        body.removeChild(div);
    }

    //Funcao para resetar a cor de todos os nos e arestas.
    function refreshGraph(){
        s.graph.nodes().forEach(function(n) {
            n.color = n.originalColor;
            n.label = n.originalLabel;
        });

        s.graph.edges().forEach(function(e) {
            e.color = e.originalColor;
        });

        s.refresh();
    }

    //Mapeamos o grafo para ser usado no algoritimo de djisktra.
    var graph_mapping = '{'
    s.graph.nodes().forEach(function(n) {
        //Guardamos a cor e o label original de cada no. 
        n.originalColor = n.color;
        n.originalLabel = n.label;

        graph_mapping += n.id + ':{'
        s.graph.edges().forEach(function(e) {
            if(e.source == n.id)
            graph_mapping += e.target + ':1,';
        });
        if(graph_mapping[graph_mapping.length -1] == ','){
            graph_mapping = graph_mapping.slice(0,-1);
            graph_mapping += '},';
        }
        else
            graph_mapping += '},';
    });
    graph_mapping = graph_mapping.slice(0,-1);
    graph_mapping += '}';

    var graph_for_dijkstra = new Graph(strToObj(graph_mapping));
    
    //Variaveis para os botões do html
    var btn_path = document.getElementById("btn-path");
    var btn_refresh = document.getElementById("btn-refresh");
    var btn_size = document.getElementById("btn-size");

    //Adiciona funcao de poder arrastar um no.
    var dragListener = sigma.plugins.dragNodes(s, s.renderers[0]);
    
    //Guardamos a cor original de todas as arestas e adicionamos a flag de nao pintadas
    s.graph.edges().forEach(function(e) {
        e.originalColor = e.color;
    });

    //Se um nó for clicado mostramos somente os seus vizinhos.
    s.bind('clickNode', function(e) {
        var nodeId = e.data.node.id,
            toKeep = s.graph.neighbors(nodeId);
            toKeep[nodeId] = e.data.node;
        
        s.graph.nodes().forEach(function(n) {
            if (toKeep[n.id]){
                n.color = n.originalColor;
                n.label = n.originalLabel;
            }
            else{
                n.label = false;
                n.color = '#eee';
            }
        });

        s.graph.edges().forEach(function(e) {
            if (toKeep[e.source] && toKeep[e.target])
                e.color = e.originalColor;
            else
                e.color = '#eee';
        });

        s.refresh();

        try {
            deleteNodeAtrributes();
            showNodeAttributes(e.data.node);
        }
        catch{
            showNodeAttributes(e.data.node);
        }
        
    });

    //Se o botao de achar caminho for clicado pintamos o caminho na tela.
    btn_path.addEventListener('click', function(e) {
        var euclidian = document.getElementById("euclidianCheckbox").checked;
        var shortest = document.getElementById("shortestCheckbox").checked;
        var destiny = document.getElementById("destiny").value;
        var target = document.getElementById("target").value;
        var path = [];

        if( (euclidian && shortest) || (!euclidian && !shortest) ){
            refreshGraph();
            return;
        }
        else if(euclidian && !shortest){
            var path1 = (s.graph.astar(destiny, target));
            for(node in path1){
                path[node] = path1[node].id;
            }
        }
        else if(!euclidian && shortest)
            path = graph_for_dijkstra.findShortestPath(destiny, target);

        refreshGraph();

        s.graph.nodes().forEach(function(n) {
            for(node in path){
                if(n.id == path[node]){
                    n.color = '#f00';
                }
            }
            if(n.color != '#f00'){
                n.label = false;
                n.color = '#eee';
            }

        });

        s.graph.edges().forEach(function(e) {
            for(i = 0; i < path.length - 1; i++){
                if(e.source == path[i] && e.target == path[i+1]){
                    e.color = '#f00';
                }
                else if(e.color != '#f00'){
                    e.color = '#eee';
                }
            }
            if(path.length == 1){
                s.graph.edges().forEach(function(e) {
                    e.color = '#eee';
                });
            }
        });

        s.refresh();
    });

    //Se o botão de refresh for clicado...
    btn_refresh.addEventListener('click', function(e) {
        refreshGraph()
        deleteNodeAtrributes();
    });

    btn_size.addEventListener('click', function(e) {
        var index = document.getElementById("select-option").selectedIndex;
        var selected = document.getElementById("select-option").options[index].text;

        s.graph.nodes().forEach(function(n) {
            n.color = n.originalColor;
            n.label = n.originalLabel;
            n.size = n.attributes[selected]*100;
        });

        s.graph.edges().forEach(function(e) {
            e.color = e.originalColor;
        });

        s.refresh();
    });

  });

