# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras

class DataBaseController:

    #Construtor que seta as configurações iniciais do banco de dados
    def __init__(self, dbname, user, host, password):
        self.dbname = dbname
        self.user = user
        self.host = host
        self.password = password
        #O metodo para conexão é chamado
        self.connect()

    #metodo para se concetar no banco de dados
    def connect(self):
        #Arruma o código que deve ser mandado para conexao
        connect_code = "dbname='" + self.dbname + "' user='" + self.user + "' host='" + self.host + "' password='" + self.password +"'"
        #Tenta se conectar
        try:
            self.conn = psycopg2.connect(connect_code)
            self.cursor = self.conn.cursor()
            print(" - Connected to the database: " + self.dbname + ".\n")
        except:
            print(" - I am unable to connect to the database: " + self.dbname + ".\n")

    #Metodo auxiliar para configurar uma parte do comando necessario para o INSERT
    def configTableCommand(self,table_name,column_order):
        if column_order:
            table_name += "("
            for att in column_order:
                if att == column_order[-1]:
                    table_name += att
                else:
                    table_name += att + ","
            table_name += ")"
        return table_name

    #Metodo auxiliar para configurar a outra parte do comando necessario para o INSERT
    def configValuesCommand(self, values_list):
        values_command = ""
        for value in values_list:
            if value == values_list[-1]:
                values_command += "'" + value + "'"
            else:
             values_command += "'" + value + "',"
        return values_command

    #Metodo para insercao na tabela
    def insertIntoTable(self, table_name, values_list,column_order=[]):

        table_command = self.configTableCommand(table_name,column_order)
        values_command = self.configValuesCommand(values_list)
        #Comando que sera executado no banco de dados atraves do psycopg
        command = "INSERT INTO " + table_command + " VALUES(" + values_command + ")"
        #Tentamos executar o comando
        try:
            self.cursor.execute(command)
        except Exception as e:
            print(e)
        self.conn.commit()

    #Metodo para contar a quantidade de linhas em uma tabela
    def countRows(self,table):
        self.cursor.execute("SELECT COUNT(*) FROM " + table)
        return int(self.cursor.fetchall()[0][0])

    #Bem simples enquanto não tiver uma ideia melhor
    def selectInTable(self,command):
        try:
            self.cursor.execute(command)
            return [list(elem) for elem in self.cursor.fetchall()]
        except Exception as e:
            print(e)


    #Retorna uma lsita com todas as tabelas do banco de dados
    def getTables(self):
        #Comando mandado para o banco de dados
        self.cursor.execute(""" SELECT table_name FROM information_schema.tables
                                WHERE table_schema = 'public'""")
        tables = self.cursor.fetchall()
        return [list(elem) for elem in tables]

    #Comando para excluir determinada tabela
    def dropTable(self,table_name):
        try:
            self.cursor.execute("DROP TABLE " + table_name)
            print("Drop table " + table_name + " successful.\n")
        except Exception as e:
            print(e)

        self.conn.commit()

    def execute(self,command):
        try:
            self.cursor.execute(command)
            return [list(elem) for elem in self.cursor.fetchall()]
        except Exception as e:
            print(e)

        self.conn.commit()
