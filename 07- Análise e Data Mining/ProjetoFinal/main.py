import csv


def getAdjacencyMatrix(csv_file):
    con_file = open(csv_file)

    content = csv.reader(con_file, delimiter=';')

    adjacency_matrix = []
    row_counter = 0
    for row in content:
        if len(row[1]) == 1:
            adjacency_matrix.append([])
            for column in row:
                if len(column) == 1:
                    adjacency_matrix[row_counter].append(int(column))

            row_counter += 1
    con_file.close()

    return adjacency_matrix



def filter_with_matrix(matrix_to_be_filtered, filter_matrix):
    for row_number, matrix_row in enumerate(filter_matrix):
        for column_number, content in enumerate(matrix_row):
            if int(content) == 0 or matrix_to_be_filtered[row_number][column_number] == 0:
                matrix_to_be_filtered[row_number][column_number] = 0
            else:
                matrix_to_be_filtered[row_number][column_number] += int(content)

    return matrix_to_be_filtered



def getNamesInOrder(csv_file):
    con_file = open(csv_file)

    content = csv.reader(con_file, delimiter=';')

    names = content.__next__()
    return names[1:]

def writeToCSVFile(names, adjacency_matrix):
    with open('similarity_artists_movies.csv', 'w+') as csv_file:
        writer = csv.writer(csv_file, delimiter = ';')
        writer.writerow([''] + names)

        name_counter = 0
        for row in adjacency_matrix:
            writer.writerow([names[name_counter]] + row)
            name_counter += 1



def main():
    adjacency_matrix_for_movies = getAdjacencyMatrix('connectivity_by_movies.csv')
    adjacency_matrix_for_artists = getAdjacencyMatrix('connectivity_by_artists.csv')
    adjacency_matrix_for_people = getAdjacencyMatrix('connectivity_by_people.csv')

    adjacency_matrix_degree_of_similarity = []
    for row in range(45):
        adjacency_matrix_degree_of_similarity.append([])
        for column in range(45):
            adjacency_matrix_degree_of_similarity[row].append(1)


    # adjacency_matrix_degree_of_similarity = filter_with_matrix(adjacency_matrix_degree_of_similarity, adjacency_matrix_for_people)
    adjacency_matrix_degree_of_similarity = filter_with_matrix(adjacency_matrix_degree_of_similarity, adjacency_matrix_for_artists)
    adjacency_matrix_degree_of_similarity = filter_with_matrix(adjacency_matrix_degree_of_similarity, adjacency_matrix_for_movies)

    names = getNamesInOrder('connectivity_by_people.csv')

    writeToCSVFile(names, adjacency_matrix_degree_of_similarity)


if __name__ == '__main__':
    main()
