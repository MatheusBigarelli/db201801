#!/bin/bash

import psycopg2
import psycopg2.extras
import os

   # Try to connect
try:
    conn = psycopg2.connect("dbname='1802LMWTechnologies' user='1802LMWTechnologies' host='200.134.10.32' password='971633'")
    print("Connected to the database.")
except:
  	print("I am unable to connect to the database.")

cur = conn.cursor()


cur.execute("""INSERT INTO Pessoa  VALUES('Jurema', 'Jurema', 'Arapongas', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Jobson', 'Jobson', 'Carapicuiba', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Popeye', 'Popeye', 'Irmaos Varner', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Olivia', 'Olivia', 'Irmaos Varner', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Mickey', 'Mickey', 'Valdisnei', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Pato Donald', 'Pato Donald', 'Valdisnei', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Daniel San', 'Daniel San', 'Casa do Mestre Miagi', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Johny Lawrence', 'Johny Lawrence', 'Cobra Kai Dojo', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Robert Downey Jr', 'Robert Downey Jr', '1088 Malibu point 90265', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Capitao America', 'Capitao America', 'O Laboratorio no esgoto', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Homem Roxo', 'Thanos', 'Um lugar triste', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Metade do universo', 'Metade do universo', 'So deus sabe', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Muleque bom da porra', 'Anakin', 'Tatooine', NULL)""")
cur.execute("""INSERT INTO Pessoa  VALUES('Obi-Wan', 'Obi-Wan', 'Algum lugar da Republica', NULL)""")


cur.execute("""INSERT INTO Conhece VALUES ('Homem Roxo', 'Robert Downey Jr')""")
cur.execute("""INSERT INTO Conhece VALUES ('Obi-Wan', 'Muleque bom da porra')""")
cur.execute("""INSERT INTO Conhece VALUES ('Johny Lawrence', 'Daniel San')""")
cur.execute("""INSERT INTO Conhece VALUES ('Capitao America', 'Robert Downey Jr')""")
cur.execute("""INSERT INTO Conhece VALUES ('Mickey', 'Pato Donald')""")
cur.execute("""INSERT INTO Conhece VALUES ('Popeye', 'Olivia')""")
cur.execute("""INSERT INTO Conhece VALUES ('Jurema', 'Jobson')""")


cur.execute("""INSERT INTO Bloqueia VALUES('Jurema', 'Jobson', 'Chingou o coleguinha')""")
cur.execute("""INSERT INTO Bloqueia VALUES('Popeye', 'Olivia', 'Nao deu espinafre')""")
cur.execute("""INSERT INTO Bloqueia VALUES('Mickey', 'Pato Donald', 'Nao chamou pro DD')""")
cur.execute("""INSERT INTO Bloqueia VALUES('Daniel San', 'Johny Lawrence', 'Vacilao') """)
cur.execute("""INSERT INTO Bloqueia VALUES('Robert Downey Jr', 'Capitao America', 'Nao gosto de você')""")
cur.execute("""INSERT INTO Bloqueia VALUES('Homem Roxo', 'Metade do universo', 'Porque eu quero')""")
cur.execute("""INSERT INTO Bloqueia VALUES('Muleque bom da porra', 'Obi-Wan', 'Me deixou cotoco')""")



cur.execute("""INSERT INTO Filme  VALUES('2', 'Avengers', '2012-04-27')""")
cur.execute("""INSERT INTO Filme  VALUES('3', 'Lord of the Rings', '2001-01-01')""")
cur.execute("""INSERT INTO Filme  VALUES('4', 'Back to the Future', '1985-12-25')""")
cur.execute("""INSERT INTO Filme  VALUES('5', '300', '2007-03-07')""")
cur.execute("""INSERT INTO Filme  VALUES('6', 'Grown Ups', '2010-09-24')""")
cur.execute("""INSERT INTO Filme  VALUES('7', 'Coração Valente', '1995-05-19')""")
cur.execute("""INSERT INTO Filme  VALUES('8', 'O Grande Ditador', '2005-09-23')""")

cur.execute("""INSERT INTO CurteFilme  VALUES('Robert Downey Jr', '2', '10')""")
cur.execute("""INSERT INTO CurteFilme  VALUES('Muleque bom da porra', '3', '4')""")
cur.execute("""INSERT INTO CurteFilme  VALUES('Obi-Wan', '4', '8')""")
cur.execute("""INSERT INTO CurteFilme  VALUES('Homem Roxo', '5', '10')""")
cur.execute("""INSERT INTO CurteFilme  VALUES('Pato Donald', '6', '11')""")
cur.execute("""INSERT INTO CurteFilme  VALUES('Jobson', '2', '7')""")
cur.execute("""INSERT INTO CurteFilme  VALUES('Johny Lawrence', '4', '6')""")

# cur.execute("""INSERT INTO ArvoreCategorias VALUES('Fantasia', NULL)""")
# cur.execute("""INSERT INTO ArvoreCategorias VALUES('Ficção Científica', 'Fantasia')""")
# cur.execute("""INSERT INTO ArvoreCategorias VALUES('Ação', NULL)""")
# cur.execute("""INSERT INTO ArvoreCategorias VALUES('Aventura', NULL)""")
# cur.execute("""INSERT INTO ArvoreCategorias VALUES('Super-Heróis', 'Ficção Científica')""")
cur.execute("""INSERT INTO ArvoreCategorias VALUES ('Terror', NULL)""")
cur.execute("""INSERT INTO ArvoreCategorias VALUES ('Ficção Histórica', 'Fantasia')""")
cur.execute("""INSERT INTO ArvoreCategorias VALUES ('Drama', NULL)""")

cur.execute("""INSERT INTO Pertence  VALUES('2', 'Ação')""")
cur.execute("""INSERT INTO Pertence  VALUES('2', 'Super-Heróis')""")
cur.execute("""INSERT INTO Pertence  VALUES('3', 'Ação')""")
cur.execute("""INSERT INTO Pertence  VALUES('4', 'Fantasia')""")
cur.execute("""INSERT INTO Pertence  VALUES('5', 'Ação')""")
cur.execute("""INSERT INTO Pertence  VALUES('4', 'Ficção Científica')""")
cur.execute("""INSERT INTO Pertence  VALUES('7', 'Ficção Histórica')""")
cur.execute("""INSERT INTO Pertence  VALUES('8', 'Drama')""")


cur.execute("""INSERT INTO Artista  VALUES('1','Jon Favreau',NULL,NULL)""")
cur.execute("""INSERT INTO Artista  VALUES('2', 'Adam Sandler', NULL,NULL)""")
cur.execute("""INSERT INTO Artista  VALUES('3', 'Scarlett', NULL, NULL)""")
cur.execute("""INSERT INTO Artista  VALUES('4', 'Michael Fox', NULL, NULL)""")
cur.execute("""INSERT INTO Artista  VALUES('5', 'Gerard Butler', NULL, NULL)""")
cur.execute("""INSERT INTO Artista  VALUES('6', 'Elijah Wood', NULL, NULL)""")
cur.execute("""INSERT INTO Artista  VALUES('7', 'Mel Gibson', NULL, NULL)""")
cur.execute("""INSERT INTO Artista  VALUES('8', 'Chaplin', NULL, NULL)""")


cur.execute("""INSERT INTO Atua VALUES ('2', '1')""")
cur.execute("""INSERT INTO Atua VALUES ('6', '2')""")
cur.execute("""INSERT INTO Atua VALUES ('2', '3')""")
cur.execute("""INSERT INTO Atua VALUES ('4', '4')""")
cur.execute("""INSERT INTO Atua VALUES ('5', '5')""")
cur.execute("""INSERT INTO Atua VALUES ('3', '6')""")
cur.execute("""INSERT INTO Atua VALUES ('7', '7')""")
cur.execute("""INSERT INTO Atua VALUES ('8', '8')""")

cur.execute("""INSERT INTO Dirige VALUES ('1', '1')""")
cur.execute("""INSERT INTO Dirige VALUES ('7', '7')""")
cur.execute("""INSERT INTO Dirige VALUES ('8', '8')""")


cur.execute("""INSERT INTO ArtistaMusical VALUES ('1', 'Beyoncé', 'R&B', 'Estados Unidos')""")
cur.execute("""INSERT INTO ArtistaMusical VALUES ('2', 'AC-DC', 'Rock and Roll', 'Autrália')""")
cur.execute("""INSERT INTO ArtistaMusical VALUES ('3', 'Black Sabath', 'Rock and Roll', 'Inglaterra')""")
cur.execute("""INSERT INTO ArtistaMusical VALUES ('4', 'One Direction', 'Pop', 'Inglaterra')""")
cur.execute("""INSERT INTO ArtistaMusical VALUES ('5', 'Avicii', 'Eletrônica', 'Suécia')""")
cur.execute("""INSERT INTO ArtistaMusical VALUES ('6', 'Shawn Mendes', 'Pop', 'Canadá')""")

cur.execute("""INSERT INTO CurteArtistaMusical VALUES ('Homem Roxo', '1', '11')""")
cur.execute("""INSERT INTO CurteArtistaMusical VALUES ('Popeye', '2', '8')""")
cur.execute("""INSERT INTO CurteArtistaMusical VALUES ('Robert Downey Jr', '4', '15')""")
cur.execute("""INSERT INTO CurteArtistaMusical VALUES ('Capitao America', '5', '6')""")

cur.execute("""INSERT INTO Musico  VALUES('1', 'Edward Sheeran', 'Pop', '1986-10-19')""")
cur.execute("""INSERT INTO Musico  VALUES('2', 'James Page', 'Rock and Roll', '959-07-08')""")
cur.execute("""INSERT INTO Musico  VALUES('3', 'John Osbourne', 'Rock and Roll', '1968-02-28')""")
cur.execute("""INSERT INTO Musico  VALUES('4', 'Angus Young', 'Rock and Roll', '1964-09-7')""")
cur.execute("""INSERT INTO Musico  VALUES('5', 'Tony Iommi', 'Rock and Roll', '1970-12-16')""")
cur.execute("""INSERT INTO Musico  VALUES('6', 'Shawn Mendes', 'Pop', '1998-04-15')""")
cur.execute("""INSERT INTO Musico  VALUES('7', 'Beyoncé', 'R&B', '1982-03-5')""")
cur.execute("""INSERT INTO Musico  VALUES('8', 'Louis Tomlinson', 'Pop', '1993-04-15')""")
cur.execute("""INSERT INTO Musico  VALUES('9', 'Tim Bergling', 'Eletrônica', '1994-07-21')""")

cur.execute("""INSERT INTO Cantor VALUES ('1', '7')""")
cur.execute("""INSERT INTO Cantor VALUES ('2', '4')""")
cur.execute("""INSERT INTO Cantor VALUES ('3', '3')""")
cur.execute("""INSERT INTO Cantor VALUES ('4', '8')""")
cur.execute("""INSERT INTO Cantor VALUES ('5', '9')""")
cur.execute("""INSERT INTO Cantor VALUES ('6', '6')""")

cur.execute("""INSERT INTO Banda VALUES ('1')""")
cur.execute("""INSERT INTO Banda VALUES ('2')""")
cur.execute("""INSERT INTO Banda VALUES ('3')""")
cur.execute("""INSERT INTO Banda VALUES ('4')""")
cur.execute("""INSERT INTO Banda VALUES ('5')""")
cur.execute("""INSERT INTO Banda VALUES ('6')""")


cur.execute("""INSERT INTO Possui VALUES ('1', '7')""")
cur.execute("""INSERT INTO Possui VALUES ('3', '3')""")
cur.execute("""INSERT INTO Possui VALUES ('2', '4')""")
cur.execute("""INSERT INTO Possui VALUES ('3', '5')""")
cur.execute("""INSERT INTO Possui VALUES ('4', '8')""")
cur.execute("""INSERT INTO Possui VALUES ('5', '9')""")

conn.commit()
