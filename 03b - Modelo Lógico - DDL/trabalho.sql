CREATE TABLE Pessoa(
              Login VARCHAR(30) NOT NULL,
              Nome VARCHAR(80) NOT NULL,
              CidadeNatal VARCHAR(30),
              DataNascimento DATE,
              PRIMARY KEY(Login)
              );

CREATE TABLE Conhece(
              Login1 VARCHAR(30) NOT NULL,
              Login2 VARCHAR(30) NOT NULL,
              PRIMARY KEY(Login1,Login2),
              FOREIGN KEY(Login1)
                  REFERENCES Pessoa(Login)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(Login2)
                  REFERENCES Pessoa(Login)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE Bloqueia(
              Login1 VARCHAR(30) NOT NULL,
              Login2 VARCHAR(30) NOT NULL,
              Razao VARCHAR(20) NOT NULL,
              PRIMARY KEY(Login1,Login2,Razao),
              FOREIGN KEY(Login1)
                  REFERENCES Pessoa(Login)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(Login2)
                  REFERENCES Pessoa(Login)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE Filme(
              ID VARCHAR(15) NOT NULL,
              Nome VARCHAR(30),
              DataLancamento DATE,
              PRIMARY KEY(ID)
              );




CREATE TABLE CurteFilme(
              Login_Pessoa VARCHAR(30) NOT NULL,
              ID_Filme VARCHAR(20) NOT NULL,
              Nota INTEGER NOT NULL,
              PRIMARY KEY(Login_Pessoa,ID_Filme),
              FOREIGN KEY(Login_Pessoa)
                  REFERENCES Pessoa(Login)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(ID_Filme)
                  REFERENCES Filme(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE ArvoreCategorias(
              Categoria VARCHAR(20) NOT NULL,
              Superior VARCHAR(20),
              PRIMARY KEY(Categoria),
              FOREIGN KEY(Superior)
                  REFERENCES ArvoreCategorias(Categoria)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE Pertence(
              ID_Filme VARCHAR(4) NOT NULL,
              Categoria VARCHAR(20) NOT NULL,
              PRIMARY KEY(ID_Filme,Categoria),
              FOREIGN KEY(ID_Filme)
                  REFERENCES Filme(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(Categoria)
                  REFERENCES ArvoreCategorias(Categoria)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE Artista(
              ID VARCHAR(4) NOT NULL,
              Nome VARCHAR(30) NOT NULL,
              Telefone VARCHAR(15),
              Endereco VARCHAR(30),
              PRIMARY KEY(ID)
              );

CREATE TABLE Atua(
              ID_Filme VARCHAR(4) NOT NULL,
              ID_Artista VARCHAR(4) NOT NULL,
              PRIMARY KEY(ID_Filme,ID_Artista),
              FOREIGN KEY(ID_Filme)
                  REFERENCES Filme(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(ID_Artista)
                  REFERENCES Artista(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE Dirige(
              ID_Filme VARCHAR(4) NOT NULL,
              ID_Artista VARCHAR(4) NOT NULL,
              PRIMARY KEY(ID_Filme),
              FOREIGN KEY(ID_Filme)
                  REFERENCES Filme(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(ID_Artista)
                  REFERENCES Artista(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE ArtistaMusical(
              ID VARCHAR(4) NOT NULL,
              NomeArtistico VARCHAR(50) NOT NULL,
              Genero VARCHAR(15),
              Pais VARCHAR(20),
              PRIMARY KEY(ID)
              );










CREATE TABLE CurteArtistaMusical(
              Login_Pessoa VARCHAR(30) NOT NULL,
              ID_ArtistaMusical VARCHAR(100) NOT NULL,
              Nota INTEGER NOT NULL,
              PRIMARY KEY(Login_Pessoa,ID_ArtistaMusical),
              FOREIGN KEY(Login_Pessoa)
                  REFERENCES Pessoa(Login)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(ID_ArtistaMusical)
                  REFERENCES ArtistaMusical(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE Musico(
              ID VARCHAR(4) NOT NULL,
              NomeReal VARCHAR(30) NOT NULL,
              EstiloMusical VARCHAR(20) NOT NULL,
              DataNascimento DATE NOT NULL,
              PRIMARY KEY(ID)
              );

CREATE TABLE Cantor(
              ID VARCHAR(4) NOT NULL,
              ID_Musico VARCHAR(30) NOT NULL,
              PRIMARY KEY(ID),
              FOREIGN KEY(ID)
                  REFERENCES ArtistaMusical(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(ID_Musico)
                  REFERENCES Musico(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE Banda(
              ID VARCHAR(4) NOT NULL,
              PRIMARY KEY(ID),
              FOREIGN KEY(ID)
                  REFERENCES ArtistaMusical(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

CREATE TABLE Possui(
              ID VARCHAR(4) NOT NULL,
              ID_Musico VARCHAR(30) NOT NULL,
              PRIMARY KEY(ID,ID_Musico),
              FOREIGN KEY(ID)
                  REFERENCES ArtistaMusical(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION,
              FOREIGN KEY(ID_Musico)
                  REFERENCES Musico(ID)
                      ON DELETE NO ACTION
                      ON UPDATE NO ACTION
              );

INSERT INTO Pessoa  VALUES('Jurema', 'Jurema', 'Arapongas', NULL);
INSERT INTO Pessoa  VALUES('Jobson', 'Jobson', 'Carapicuiba', NULL);
INSERT INTO Pessoa  VALUES('Popeye', 'Popeye', 'Irmaos Varner', NULL);
INSERT INTO Pessoa  VALUES('Olivia', 'Olivia', 'Irmaos Varner', NULL);
INSERT INTO Pessoa  VALUES('Mickey', 'Mickey', 'Valdisnei', NULL);
INSERT INTO Pessoa  VALUES('Pato Donald', 'Pato Donald', 'Valdisnei', NULL);
INSERT INTO Pessoa  VALUES('Daniel San', 'Daniel San', 'Casa do Mestre Miagi', NULL);
INSERT INTO Pessoa  VALUES('Johny Lawrence', 'Johny Lawrence', 'Cobra Kai Dojo', NULL);
INSERT INTO Pessoa  VALUES('Robert Downey Jr', 'Robert Downey Jr', '1088 Malibu point 90265', NULL);
INSERT INTO Pessoa  VALUES('Capitao America', 'Capitao America', 'O Laboratorio no esgoto', NULL);
INSERT INTO Pessoa  VALUES('Homem Roxo', 'Thanos', 'Um lugar triste', NULL);
INSERT INTO Pessoa  VALUES('Metade do universo', 'Metade do universo', 'So deus sabe', NULL);
INSERT INTO Pessoa  VALUES('Muleque bom da porra', 'Anakin', 'Tatooine', NULL);
INSERT INTO Pessoa  VALUES('Obi-Wan', 'Obi-Wan', 'Algum lugar da Republica', NULL);


INSERT INTO Conhece VALUES ('Homem Roxo', 'Robert Downey Jr');
INSERT INTO Conhece VALUES ('Obi-Wan', 'Muleque bom da porra');
INSERT INTO Conhece VALUES ('Johny Lawrence', 'Daniel San');
INSERT INTO Conhece VALUES ('Capitao America', 'Robert Downey Jr');
INSERT INTO Conhece VALUES ('Mickey', 'Pato Donald');
INSERT INTO Conhece VALUES ('Popeye', 'Olivia');
INSERT INTO Conhece VALUES ('Jurema', 'Jobson');


INSERT INTO Bloqueia VALUES('Jurema', 'Jobson', 'Chingou o coleguinha');
INSERT INTO Bloqueia VALUES('Popeye', 'Olivia', 'Nao deu espinafre');
INSERT INTO Bloqueia VALUES('Mickey', 'Pato Donald', 'Nao chamou pro DD');
INSERT INTO Bloqueia VALUES('Daniel San', 'Johny Lawrence', 'Vacilao') ;
INSERT INTO Bloqueia VALUES('Robert Downey Jr', 'Capitao America', 'Nao gosto de você');
INSERT INTO Bloqueia VALUES('Homem Roxo', 'Metade do universo', 'Porque eu quero');
INSERT INTO Bloqueia VALUES('Muleque bom da porra', 'Obi-Wan', 'Me deixou cotoco');

INSERT INTO Filme VALUES('1','Iron Man','2008-04-14') ;
INSERT INTO Filme  VALUES('2', 'Avengers', '2012-04-27');
INSERT INTO Filme  VALUES('3', 'Lord of the Rings', '2001-01-01');
INSERT INTO Filme  VALUES('4', 'Back to the Future', '1985-12-25');
INSERT INTO Filme  VALUES('5', '300', '2007-03-07');
INSERT INTO Filme  VALUES('6', 'Grown Ups', '2010-09-24');
INSERT INTO Filme  VALUES('7', 'Coração Valente', '1995-05-19');
INSERT INTO Filme  VALUES('8', 'O Grande Ditador', '2005-09-23');

INSERT INTO CurteFilme  VALUES('Robert Downey Jr', '2', '10');
INSERT INTO CurteFilme  VALUES('Muleque bom da porra', '3', '4');
INSERT INTO CurteFilme  VALUES('Obi-Wan', '4', '8');
INSERT INTO CurteFilme  VALUES('Homem Roxo', '5', '10');
INSERT INTO CurteFilme  VALUES('Pato Donald', '6', '11');
INSERT INTO CurteFilme  VALUES('Jobson', '2', '7');
INSERT INTO CurteFilme  VALUES('Johny Lawrence', '4', '6');

INSERT INTO ArvoreCategorias VALUES('Fantasia', NULL);
INSERT INTO ArvoreCategorias VALUES('Ficção Científica', 'Fantasia');
INSERT INTO ArvoreCategorias VALUES('Ação', NULL);
INSERT INTO ArvoreCategorias VALUES('Aventura', NULL);
INSERT INTO ArvoreCategorias VALUES('Super-Heróis', 'Ficção Científica');
INSERT INTO ArvoreCategorias VALUES ('Terror', NULL);
INSERT INTO ArvoreCategorias VALUES ('Ficção Histórica', 'Fantasia');
INSERT INTO ArvoreCategorias VALUES ('Drama', NULL);

INSERT INTO Pertence VALUES('1','Ação');
INSERT INTO Pertence VALUES('1','Aventura');
INSERT INTO Pertence VALUES('1','Super-Heróis');
INSERT INTO Pertence  VALUES('2', 'Ação');
INSERT INTO Pertence  VALUES('2', 'Super-Heróis');
INSERT INTO Pertence  VALUES('3', 'Ação');
INSERT INTO Pertence  VALUES('4', 'Fantasia');
INSERT INTO Pertence  VALUES('5', 'Ação');
INSERT INTO Pertence  VALUES('4', 'Ficção Científica');
INSERT INTO Pertence  VALUES('7', 'Ficção Histórica');
INSERT INTO Pertence  VALUES('8', 'Drama');


INSERT INTO Artista  VALUES('1','Jon Favreau',NULL,NULL);
INSERT INTO Artista  VALUES('2', 'Adam Sandler', NULL,NULL);
INSERT INTO Artista  VALUES('3', 'Scarlett', NULL, NULL);
INSERT INTO Artista  VALUES('4', 'Michael Fox', NULL, NULL);
INSERT INTO Artista  VALUES('5', 'Gerard Butler', NULL, NULL);
INSERT INTO Artista  VALUES('6', 'Elijah Wood', NULL, NULL);
INSERT INTO Artista  VALUES('7', 'Mel Gibson', NULL, NULL);
INSERT INTO Artista  VALUES('8', 'Chaplin', NULL, NULL);


INSERT INTO Atua VALUES ('2', '1');
INSERT INTO Atua VALUES ('6', '2');
INSERT INTO Atua VALUES ('2', '3');
INSERT INTO Atua VALUES ('4', '4');
INSERT INTO Atua VALUES ('5', '5');
INSERT INTO Atua VALUES ('3', '6');
INSERT INTO Atua VALUES ('7', '7');
INSERT INTO Atua VALUES ('8', '8');

INSERT INTO Dirige VALUES ('1', '1');
INSERT INTO Dirige VALUES ('7', '7');
INSERT INTO Dirige VALUES ('8', '8');


INSERT INTO ArtistaMusical VALUES ('1', 'Beyoncé', 'R&B', 'Estados Unidos');
INSERT INTO ArtistaMusical VALUES ('2', 'AC-DC', 'Rock and Roll', 'Autrália');
INSERT INTO ArtistaMusical VALUES ('3', 'Black Sabath', 'Rock and Roll', 'Inglaterra');
INSERT INTO ArtistaMusical VALUES ('4', 'One Direction', 'Pop', 'Inglaterra');
INSERT INTO ArtistaMusical VALUES ('5', 'Avicii', 'Eletrônica', 'Suécia');
INSERT INTO ArtistaMusical VALUES ('6', 'Shawn Mendes', 'Pop', 'Canadá');

INSERT INTO CurteArtistaMusical VALUES ('Homem Roxo', '1', '11');
INSERT INTO CurteArtistaMusical VALUES ('Popeye', '2', '8');
INSERT INTO CurteArtistaMusical VALUES ('Robert Downey Jr', '4', '15');
INSERT INTO CurteArtistaMusical VALUES ('Capitao America', '5', '6');

INSERT INTO Musico  VALUES('1', 'Edward Sheeran', 'Pop', '1986-10-19');
INSERT INTO Musico  VALUES('2', 'James Page', 'Rock and Roll', '959-07-08');
INSERT INTO Musico  VALUES('3', 'John Osbourne', 'Rock and Roll', '1968-02-28');
INSERT INTO Musico  VALUES('4', 'Angus Young', 'Rock and Roll', '1964-09-7');
INSERT INTO Musico  VALUES('5', 'Tony Iommi', 'Rock and Roll', '1970-12-16');
INSERT INTO Musico  VALUES('6', 'Shawn Mendes', 'Pop', '1998-04-15');
INSERT INTO Musico  VALUES('7', 'Beyoncé', 'R&B', '1982-03-5');
INSERT INTO Musico  VALUES('8', 'Louis Tomlinson', 'Pop', '1993-04-15');
INSERT INTO Musico  VALUES('9', 'Tim Bergling', 'Eletrônica', '1994-07-21');

INSERT INTO Cantor VALUES ('1', '7');
INSERT INTO Cantor VALUES ('2', '4');
INSERT INTO Cantor VALUES ('3', '3');
INSERT INTO Cantor VALUES ('4', '8');
INSERT INTO Cantor VALUES ('5', '9');
INSERT INTO Cantor VALUES ('6', '6');

INSERT INTO Banda VALUES ('1');
INSERT INTO Banda VALUES ('2');
INSERT INTO Banda VALUES ('3');
INSERT INTO Banda VALUES ('4');
INSERT INTO Banda VALUES ('5');
INSERT INTO Banda VALUES ('6');

INSERT INTO Possui VALUES ('1', '7');
INSERT INTO Possui VALUES ('3', '3');
INSERT INTO Possui VALUES ('2', '4');
INSERT INTO Possui VALUES ('3', '5');
INSERT INTO Possui VALUES ('4', '8');
INSERT INTO Possui VALUES ('5', '9');
