# Estrutura básica do trabalho de CSB30 - Introdução A Bancos De Dados

Modifique este aquivo README.md com as informações adequadas sobre o seu grupo.

## Integrantes do grupo

Liste o nome, RA e o usuário GitLab para cada integrante do grupo.

- Lucas da Silva Nolasco,            1860240, lucasnolasco
- Matheus Bigarelli Dantas da Costa, 1828568, MatheusBigarelli
- Wagner Rodrigues Ulian Agostinho,  1860364, wagner.rua

## Descrição da aplicação a ser desenvolvida 

 - O que sera implementado:

Uma aplicação web para apresentação da analise dos dados adquiridos através de grafos.

- Quais tecnologias serão utilizadas:

Gephi(Analise de grafos), Django (Framework para desenvolvimento web), Sigma.js (Apresentação web de grafos)

- Por que decidimos pelo tema:

Pois o uso de grafos e o desenvolvimento web estão muito presentes nos dias de hoje, o primeiro modela diversos tipos de problemas, em especial para redes sociais como é o caso desse trabalho em específico e o segundo está presente no nosso dia-a-dia sempre que usamos o navegador seja no celular ou no computador.

- Desafios relacionados com bancos de dados (consultas, modelos, etc.):

Para fazer a análise com grafos vamos ter que fazer consultas mais especificas do que as tabelas com os dados que temos, além disso temos que saber quais dados escolher para tirar as conclusões que pretendemos. Vale um destaque a consulta recursiva que queremos utilizar que teve de ser aprendida por conta própria, depois de uma dica do professor.

- Desafios relacionados com as áreas de interesse:

Não temos um conhecimento muito aprofundado em grafos e desenvolvimento web, além de ser necessário um bom planejamento para escolhermos os dados certos a serem analisados. Outro desafio era fazer uma página interativa para que o aprendizado e a análise com grafos seja mais interessante.

- Conhecimentos novos que pretendemos adquirir:

Pretendemos aprimorar o conhecimento em grafos e em desenvolvimento web, através das ferramentas mencionadas acima.

- O que já foi produzido (protótipos, esquemas de telas, teste de codificação de partes críticas, etc.):

Uma aplicação web contendo três páginas para análise e interação com os dados adquiridos em forma de grafos. A página principal é composta por um grafo que contém as ligações entre os alunos conhecidos da turma, atráves desse grafo é possível encontrar o caminho mínimo entre dois estudantes através do algoritimo de Dijkstra e o menor caminho euclidiano com o algoritimo A*, também deve ser possível a vizualização das ligações de um nó quando ele é clicado (além do mesmo poder ser arrastado pela tela), outra funcionalidade é a mudança de tamanho dos nós de acordo com o atributo selecionado pelo usuário. Na segunda página temos a análise multilayer de alguns dados, utilizando o gosto musical, o gosto por filmes e o mesmo grafo utilizado na primeira página é possivel encontrar grupos conectados por mais de uma forma, então nessa segunda página são apresentados um total de 7 grafos onde é possivel vizualizar desde as ligações basicas até a ligação mais profunda entre usuários da rede que em todas característica em comum. Por último temos a página que usa uma consulta recursiva em SQL para criar uma grafo em tempo real da quantidade pessoas que um determinado usuário pode alcançar com um certo número de passos. Cabe ressaltar que os grafos utilizados nas duas primeiras páginas foram feitos utilizando a ferramenta Gephi através de consultas no nosso banco de dados.

## Perguntas aos Desenvolvedores

- Nome: Wagner Rodrigues Ulian Agostinho

- Qual o seu foco na implementação do trabalho?

Meu foco é aprender a aplicar grafos em problemas reais a partir de informações em um determinado banco de dados.

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?

Tenho um pouco de conhecimento com o uso de Django, pois estou usando ele para fazer um trabalho de outra disciplina, conheço um pouco sobre o Gephi, mas nunca use ele
na prática.

- Qual aspecto do trabalho te interessa mais?

O uso de grafos me interessa muito, a quantidade de informações e conclusões que podem ser tiradas de grafos são muito grandes e unindo isso com uma aplicação web
é possível dar interatividade a essa estrutura de dados.



- Nome: Lucas da Silva Nolasco

- Qual o seu foco na implementação do trabalho?

Obter experiência em um problema real envolvendo a aplicação de grafos.

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?

Não

- Qual aspecto do trabalho te interessa mais?

A possibilidade de aprender mais sobre a implementação prática de problemas envolvendo grafos.



- Nome: Matheus Bigarelli Dantas da Costa

- Qual seu foco na implementação do trabalho?

Unir as disciplinas de algoritmos e banco de dados e conseguir uma oportunidade de ver uma aplicação "do mundo real" acerca dos assuntos.

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais?

Ainda não.

- Qual aspecto do trabalho te interessa mais?

O fato de podermos sair um pouco do contexto das aulas e, em vez de usarmos bancos de dados relacionais, podermos usar um banco de dados baseados em grafos e ainda poder sair um pouco do monotonia de fazer consultas preparadas já e poder realmente extrair alguma informação de um ambiente que frequentamos todos os dias.
