# -*- coding: utf-8 -*-
import psycopg2
import os
from databaseController import DataBaseController

REGISTER = 1
LIST = 2
DELETE = 1
UPDATE = 2
BACK = 3
EXIT = 5

def main():
    opcao = 0
    extras = 0

    while opcao != EXIT:
        if opcao != LIST and not extras:
            limpaTela()
        extras = disponibilizarOpcoesExtras(opcao, extras)
        imprimirOpcoes(extras)
        opcao = pegarOpcao(extras)
        executarOpcao(opcao, extras)


def limpaTela():
    try:
        os.system('clear')
    except:
        os.system('cls')


def imprimirOpcoes(extras):
    print '\n\n'
    print '*' * 15 + ' Interface CRUD ' + '*' * 15
    print 'Opcoes:'
    if not extras:
        print '1 - Cadastrar.'
        print '2 - Listar.'
        print '5 - Sair.'
    if extras:
        print '1 - Deletar.'
        print '2 - Atualziar.'
        print '3 - Voltar.'
        print '5 - Sair.'
        
    

def disponibilizarOpcoesExtras(ultima_opcao, extras):
    if ultima_opcao == LIST and extras == False:
        extras = True
    else:
        extras = False

    return extras

def pegarOpcao(extras):
    opcao = 7
    if not extras:
        while opcao not in [REGISTER, LIST, EXIT]:
            opcao = int(input('Escolha uma opcao: '))
    else:
        while opcao not in (DELETE, UPDATE, EXIT, BACK):
            opcao = int(input('Escolha uma opcao: '))

    return opcao

def executarOpcao(opcao,extras):

    if opcao == REGISTER and not extras:
        criarUsuarioNoBanco()
    elif opcao == LIST and not extras:
        limpaTela()
        print '-' * 50 + "Todas Pessoas Do Sistema" + '-' * 50
        lerBanco()
    elif opcao == DELETE and extras:
        deletarInformacaoDoBanco()
    elif opcao == UPDATE and extras:
        atualizarBanco()
    elif opcao == BACK and extras:
        print "Back"
        extras = False



def criarUsuarioNoBanco():
    print 'Criando novo usuario...'

    database_controller = criarConexaoComBanco()

    novo_usuario = pegarInformacoesDoNovoUsuario()
    inserirNovoUsuarioNoBanco(database_controller, novo_usuario)


def criarConexaoComBanco():
    dbname = '1802LMWTechnologies'
    user = '1802LMWTechnologies'
    host = '200.134.10.32'
    password = '971633'

    database_controller = DataBaseController(dbname, user, host, password)
    database_controller.connect()
    return database_controller


def pegarInformacoesDoNovoUsuario():
    nome = ''
    while len(nome) == 0 or len(nome) > 80:
        nome = raw_input('Insira o nome do novo usuario: ')
        if nome == None:
            nome = ''

    login = raw_input('Insira o login do novo usuario: ')
    cidade_natal = raw_input('Insira a cidade natal do novo usuario: ')
    if not cidade_natal:
        cidade_natal = 'NULL'
    data_de_nascimento = raw_input('Insira a data de nascimento do novo usuario (aaaa-mm-dd): ')
    if not data_de_nascimento:
        data_de_nascimento = 'NULL'

    novo_usuario = []
    novo_usuario.append(login)
    novo_usuario.append(nome)
    novo_usuario.append(cidade_natal)
    novo_usuario.append(data_de_nascimento)

    return novo_usuario

def inserirNovoUsuarioNoBanco(database_controller, novo_usuario):
    tabela = 'Pessoa'
    print novo_usuario
    database_controller.insertIntoTable(tabela, novo_usuario)


def lerBanco():
    database_controller = criarConexaoComBanco()
    pegaListaDePessoasNoBanco(database_controller)


def pegaListaDePessoasNoBanco(database_controller):
    tabela = 'Pessoa'
    resposta = database_controller.anotherSelect(tabela)
    imprimeResposta(resposta)

def imprimeResposta(resposta):
    
    for linha in resposta:
        if linha:
            print "Login: " + str(linha[0]) + " - Nome: " + str(linha[1]) + " - CidadeNatal: " + str(linha[2]) + " - DataNascimento: " + str(linha[3])

def atualizarBanco():
    print 'Atualizando...'
    login = raw_input('Digite o login da pessoa que sera atualizada: ')
    opcao_selecionada = raw_input('Digite a informacao que sera alterada (Login, Nome, CidadeNatal, DataNascimento): ')
    novo_valor = raw_input('Digite o novo valor para a coluna: ')
    

    database_controller = criarConexaoComBanco()
    database_controller.updateTable('Pessoa', opcao_selecionada, novo_valor, 'login', login)



def deletarInformacaoDoBanco():
    print 'Deletando do banco...'
    usuario_a_ser_deletado = pegarUsuarioASerDeletado()

    database_controller = criarConexaoComBanco()

    tabela = 'Pessoa'

    database_controller.deleteFromTable(tabela, 'login', usuario_a_ser_deletado)


def pegarUsuarioASerDeletado():
	login_do_usuario = raw_input('Digite o login de usuario a ser deletado: ')
	return login_do_usuario


if __name__ == '__main__':
    main()
