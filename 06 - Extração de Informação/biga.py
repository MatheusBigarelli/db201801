# -*- coding: utf-8 -*-

import wptools
import re
import xml.etree.ElementTree as ET
from lxml import etree

from dataBaseController import DataBaseController

def correctName(name):
    name = name.replace('%C3%A7', u'ç')
    name = name.replace('%C3%A9', u'é')
    name = name.replace('%C3%B3', u'ó')
    name = name.replace('\\xe3', u'ã')
    name = name.replace('\\xf6', u'ö')
    name = name.replace('%27', u'\'')
    name = name.replace('%26', u'&')

    return name

def getBandNamesFromDatabase():
    db = DataBaseController('1802LMWTechnologies', '1802LMWTechnologies', '200.134.10.32', '971633')
    urls = db.selectInTable('SELECT id FROM artistaMusical')

    names = []
    for url in urls:
        name = re.sub('https://en.wikipedia.org/wiki/', '', url[0])
        name = correctName(name)
        names.append(name)

    return names


def parseContent(content):
    data = ''
    try:
        if content == '':
            print 'Empty string'


        if content[0] == '{':
            raw_data = content.split('\n')
            for string in raw_data:
                lowered = string.lower()
                index = lowered.find('genre')
                if index != -1:
                    data = lowered.split('[[')[1].split('<')[0].split('{')[0].split('{{')[0].split('{{')[0]
                    data = data.replace(']]', '').replace('|', ',').replace('}}', '').replace('|', ',')

        else:
            data = content

        if len(data) > 90:
            data = data.split(' ')
            data = data[0] + data[1] + data[2]

        data = data.replace('[', '')
        data = data.replace(']', '')


    except Exception as e:
        print e

    return data


def writeToXMLFile(bands):
    xml_file = open('music.xml', 'w')
    root = etree.Element('AllBands')
    for band in bands:
        try:
            etree.SubElement(root, 'Band', id=band['id'], nomeartistico=band['nomeartistico'], genero=band['genero'], pais=band['pais'])
        except Exception as e:
            print e
    tree = etree.ElementTree(root)
    tree.write('music.xml', encoding='utf-8', xml_declaration=True, pretty_print=True)


def updateDatabase(bands):
    db = DataBaseController('1802LMWTechnologies', '1802LMWTechnologies', '200.134.10.32', '971633')
    for band in bands:
        print band.keys()
        db.updateTable('artistaMusical', band['id'], band)

def main():
    band_names = getBandNamesFromDatabase()
    counter = 0

    out_file = open('bands.txt', 'w')

    bands = []
    for band_name in band_names:
        try:
            page = wptools.page(band_name, silent=True)

            page.get_parse()

            keys_of_interest = ['name', 'origin', 'born', 'genre', 'module', 'Born', 'Genre']

            out_file.write("Band %d:\n" %counter)

            print '\n\n'
            print 'Band: %d' % counter

            band_id = 'https://en.wikipedia.org/wiki/' + band_name
            band = {'id':band_id}
            out_file.write('id -> ' + band['id'] + '\n')
            for key in keys_of_interest:
                try:
                    content = page.data['infobox'][key]
                    data = parseContent(content)
                    if not data:
                        data = ''
                    if key == 'module':
                        band['genero'] = data
                        out_file.write('genero' + ' -> ' + band['genero'] + '\n')
                    elif key == 'origin' or key == 'born' or key == 'Born':
                        band['pais'] = data
                        out_file.write('pais' + ' -> ' + band['pais'] + '\n')
                    elif key == 'name':
                        band['nomeartistico'] = data
                        out_file.write('nomeartistico' + ' -> ' + band['nomeartistico'] + '\n')
                    elif key == 'genre' or key == 'Genre':
                        band['genero'] = data
                        out_file.write('genero' + ' -> ' + band['genero'] + '\n')

                except Exception as e:
                    print e
            out_file.write('\n\n\n')


            bands.append(band)
            print band
            counter += 1


        except Exception as e:
            print e

    out_file.close()

    writeToXMLFile(bands)
    updateDatabase(bands)

if __name__ == '__main__':
    main()
