#-*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
import datetime

def getTitle(page_soup):
    title = None

    div = page_soup.find_all('title')
    element = div[0]
    title = element.text.split(' (',1)[0]

    return(title)

def getReleaseDate(page_soup):
    release_date = None

    div = page_soup.find_all('div', class_='txt-block')
    for element in div:
        if 'Release Date:' in element.text:
            release_date = element.text.replace('Release Date:','')
            release_date = release_date.replace('\n','')
            release_date = release_date.split(' (', 1)[0]

    # Convert Data Format to YYYY-MM-DD
    release_date = datetime.datetime.strptime(release_date, ' %d %B %Y')
    release_date = release_date.strftime('%Y-%m-%d')
    return(release_date)

def getDirector(page_soup):
    director = None

    div = page_soup.find_all('div', class_='credit_summary_item')
    for element in div:
        if 'Director' in element.text:
            director = element.text.replace('Director:','')
            director = director.replace('\n','')
        if 'Directors' in element.text:
            director = element.text.replace('Directors:','')
            director = director.replace('\n','')
            director = director.split(', ')

    return director

def getListOfGenres(page_soup):
    genres = None

    div = page_soup.find_all('div', class_='see-more inline canwrap')
    for element in div:
        if 'Genres' in element.text:
            genres = element.text.replace('Genres:','')
            genres = genres.replace('\n','') # Remove line-breaks
            genres = genres.replace('|','') # Replace | for ,
            genres = genres.replace(' ','') # Remove spaces

    # Split function remover non-breaking spaces automatic
    return genres.split()

def getBudget(page_soup):
    budget = None

    table = page_soup.find('table', class_='infobox vevent')
    table_rows = table.find_all('tr')

    for tr in table_rows:
        if 'Budget' in tr.text:
            budget = tr.text.replace('Budget','')
            budget = budget.replace('\n','')
            if '[' in budget:
                budget = budget.split('[',1)[0]

    return budget


def getInfoBox(page_soup):
    infobox = page_soup.find('table', class_='infobox vcard plainlist')
    print infobox
    strs = infobox.split('<td>')
    print



def main():

    # To get info in en-US
    headers = {"Accept-Language": "en-US, en;q=0.5"}
    # url = 'http://www.imdb.com/title/tt0071562/'
    # page = requests.get(url, headers=headers)
    # soup = BeautifulSoup(page.text, 'lxml')
    #
    # print(getTitle(soup))
    # print(getReleaseDate(soup))
    # print(getListOfGenres(soup))
    # print(getDirector(soup))
    #
    # url = 'https://en.wikipedia.org/wiki/' + getTitle(soup).replace(' ', '_')
    # page = requests.get(url, headers=headers)
    # soup = BeautifulSoup(page.text, 'lxml')
    #
    # print(getBudget(soup))

    url = 'https://en.wikipedia.org/wiki/AC/DC'
    page = requests.get(url, headers=headers)
    soup = BeautifulSoup(page.text, 'lxml')
    getInfoBox(soup)


if __name__ == '__main__':
    main()
