#-*- coding: utf-8 -*-
import requests
from bs4 import BeautifulSoup
from dataBaseController import DataBaseController
import datetime
import pickle


#SE QUISER PEGAR OS DADOS DA INTERNET PARA SEREM SALVOS EM UM ARQUIVO getData = True
getData = True
#SE QUISER JOGAR OS DADOS NO BANCO DE DADOS A PARTIR DA LEITURA DOS ARQUIVOS setData = True
setData = False


class Film:
    def __init__(self, id, title, release_date, genre, director, star):
        self.id = id
        self.title = title
        self.realease_date = release_date
        self.genre = genre
        self.director = director
        self.star = star


def getTitle(page_soup):
    title = None

    div = page_soup.find_all('title')
    element = div[0]
    title = element.text.split(' (',1)[0]

    return(title)

def getReleaseDate(page_soup):
    release_date = None

    div = page_soup.find_all('div', class_='txt-block')
    for element in div:
        if 'Release Date:' in element.text:
            release_date = element.text.replace('Release Date:','')
            release_date = release_date.replace('\n','')
            release_date = release_date.split(' (', 1)[0]

    # Convert Data Format to YYYY-MM-DD
    release_date = datetime.datetime.strptime(release_date, ' %d %B %Y') 
    release_date = release_date.strftime('%Y-%m-%d')
    return(release_date)

def getDirector(page_soup):
    director = None

    result = []
    div = page_soup.find_all('div', class_='credit_summary_item')
    for element in div:
        if 'Director' in element.text:
            director = element.text.replace('Director:','')
            director = director.replace('\n','')
            result.append(director)
        if 'Directors' in element.text:
            director = element.text.replace('Directors:','')
            director = director.replace('\n','')
            director = director.split(', ')
            result = director

    list_of_directors = []
    for director in result:
        if '(' in director:
            director = director.split(' (', 1)[0]
        if '|' in director:
            director = director.split(' |', 1)[0]
        if director[-1] == ' ':
            new = list(director)
            new[-1] = ''
            director = ''.join(new)
        list_of_directors.append(director)

    return list_of_directors

def getListOfGenres(page_soup):
    genres = None

    div = page_soup.find_all('div', class_='see-more inline canwrap')
    for element in div:
        if 'Genres' in element.text:
            genres = element.text.replace('Genres:','')
            genres = genres.replace('\n','') # Remove line-breaks
            genres = genres.replace('|','') # Replace | for ,
            genres = genres.replace(' ','') # Remove spaces

    # Split function remover non-breaking spaces automatic
    return genres.split()

def getStarring(page_soup):
    starring = None

    table = page_soup.find('table', class_='infobox vevent')
    table_rows = table.find_all('tr')

    for tr in table_rows:
        if 'Starring' in tr.text:
            starring = tr.text.replace('Starring','')
            starring = starring.split('\n')


    result = []
    for star in starring:
        if star != u'':
	        result.append(star)

    return result

def setArtistsInDB(list_of_directors, list_of_stars, database):
    
    id = 9
    for director in list_of_directors:
        database.insertIntoTable('Artista',[str(id), '', '', str(director)])
        id += 1
    
    for star in list_of_stars:
        database.insertIntoTable('Artista',[str(id), '', '', str(star)])
        id += 1
    
def setFilmInDB(list_of_films, database):
    
    for film in list_of_films:
        database.execute('''UPDATE Filme
                            SET nome = \'''' + str(film.title) + 
                         '''\', datalancamento = \'''' + str(film.realease_date) +
                         '''\' WHERE id = \'''' + str(film.id) + '\'')

def setDirectorsOfFilms(list_of_films, database):

    for film in list_of_films:
        for director in film.director:
            id_director = (database.selectInTable('SELECT id FROM Artista WHERE nome = \'' + str(director) + '\''))
            database.insertIntoTable('Dirige',[str(film.id), str(id_director[0][0])])

def setStarsOfFilms(list_of_films, database):
    for film in list_of_films:
        if film.star != None:
            for star in film.star:
                if '\'' in star:
                    pass
                else:
                    id_star = (database.selectInTable('SELECT id FROM Artista WHERE nome = \'' + str(star) + '\''))
                    database.insertIntoTable('Atua',[str(film.id), str(id_star[0][0])])

def setArvoreCategorias(list_of_genres, database):
    for genre in list_of_genres:
        database.insertIntoTable('ArvoreCategorias',[str(genre)])

def setGenresOfFilms(list_of_films, database):
    for film in list_of_films:
        for genre in film.genre:
            database.insertIntoTable('Pertence',[str(film.id), str(genre)])


def main():

    database = DataBaseController("1802LMWTechnologies", "1802LMWTechnologies", "200.134.10.32", "971633")

    if getData == True:
        list_of_films_id = database.selectInTable('SELECT id FROM Filme')

        list_of_film_urls = []
        for film_id in list_of_films_id:
            list_of_film_urls.append('http://www.imdb.com/title/' + film_id[0] + '/')

        headers = {"Accept-Language": "en-US, en;q=0.5"}

        list_of_films = []
        list_of_genres = []
        list_of_directors = []
        list_of_stars = []

        #Busca os dados nos sites 
        for url in list_of_film_urls:
            page = requests.get(url, headers=headers)
            if page.status_code != 404:
                soup = BeautifulSoup(page.text, 'lxml')
                id = url.replace('http://www.imdb.com/title/','')
                id = id.replace('/','')
                title = getTitle(soup)
                release_date = getReleaseDate(soup)
                genres = getListOfGenres(soup)
                directors = getDirector(soup)
            
                url = 'https://en.wikipedia.org/wiki/' + title.replace(' ', '_')
                page = requests.get(url, headers=headers)
                soup = BeautifulSoup(page.text, 'lxml')

                stars = None
                try:
                    stars = getStarring(soup)
                    if len(stars) == 1:
                        stars = None
                except:
                    url = 'https://en.wikipedia.org/wiki/' + title.replace(' ', '_') + '_(film)'
                    page = requests.get(url, headers=headers)
                    soup = BeautifulSoup(page.text, 'lxml')

                    try:
                        stars = getStarring(soup)
                        if len(stars) == 1:
                            stars = None
                    except:
                        pass
                

                #Guarda todos os dados em listas para ficar mais facil 
                # de jogar no banco
                film = Film(id, title, release_date, genres, directors, stars)
                list_of_films.append(film)

                for genre in genres:
                    list_of_genres.append(genre)

                if stars != None:
                    for star in stars:
                        list_of_stars.append(star)

                for director in directors:
                    list_of_directors.append(director)

        print('--------------------------------------------------')
        #Para tirar os elementos repetidos
        list_of_directors = list(set(list_of_directors))
        list_of_genres = list(set(list_of_genres))
        list_of_stars = list(set(list_of_stars))

        #Salva dados em uma arquivo
        with open('./lists/list_of_films', 'wb') as fp:
            pickle.dump(list_of_films, fp)

        with open('./lists/list_of_directors', 'wb') as fp:
            pickle.dump(list_of_directors, fp)

        with open('./lists/list_of_genres', 'wb') as fp:
            pickle.dump(list_of_genres, fp)

        with open('./lists/list_of_stars', 'wb') as fp:
            pickle.dump(list_of_stars, fp)

    if setData == True:
        #Pega os dados dos arquivos
        with open('list_of_films', 'rb') as fp:
            list_of_films = pickle.load(fp)

        with open('list_of_directors', 'rb') as fp:
            list_of_directors = pickle.load(fp)

        with open('list_of_genres', 'rb') as fp:
            list_of_genres = pickle.load(fp)

        with open('list_of_stars', 'rb') as fp:
            list_of_stars = pickle.load(fp)

        #setDirectorsOfFilms(list_of_films,database)
        #setStarsOfFilms(list_of_films,database)
        #setArvoreCategorias(list_of_genres,database)
        #setGenresOfFilms(list_of_films,database)

if __name__ == '__main__':
    main()
