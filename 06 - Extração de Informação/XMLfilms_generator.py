import pickle
from xml.etree.ElementTree import Element, SubElement, Comment
from xml.etree import ElementTree
from xml.dom import minidom


class Film:
    def __init__(self, id, title, release_date, genre, director, star):
        self.id = id
        self.title = title
        self.realease_date = release_date
        self.genre = genre
        self.director = director
        self.star = star

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def main():
    with open('./lists/list_of_films', 'rb') as fp:
        list_of_films = pickle.load(fp)

    movies = Element('Movies')

    for film in list_of_films:
        movie = SubElement(movies, 'Movie', id=str(film.id), name=str(film.title), release_date=str(film.realease_date))
        for director in film.director:
            director = SubElement(movie,'Director', name=str(director))
        for genre in film.genre:
            genre = SubElement(movie,'Genre', name=str(genre))
        if film.star != None:
            for star in film.star:
                star = SubElement(movie,'Star', name=str(star))
    
    fp = open('movies.xml', 'w')
    fp.write(prettify(movies))
    fp.close

if __name__ == '__main__':
    main()