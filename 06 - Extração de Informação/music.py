# -*- coding: utf-8 -*-


from bs4 import BeautifulSoup
import requests
import re
from lxml import etree
import pylast

from dataBaseController import DataBaseController

def correctName(name):
    name = name.replace('%C3%Aa', u'á')
    name = name.replace('%C3%A3', u'ã')
    name = name.replace('%C3%A7', u'ç')
    name = name.replace('%C3%A9', u'é')
    name = name.replace('%C3%B3', u'ó')
    name = name.replace('\\xe2', u'â')
    name = name.replace('\\xe3', u'ã')
    name = name.replace('\\xf6', u'ö')
    name = name.replace('%27', u'\'')
    name = name.replace('%26', u'&')

    return name

def getBandNamesFromDatabase():
    db = DataBaseController('1802LMWTechnologies', '1802LMWTechnologies', '200.134.10.32', '971633')
    urls = db.selectInTable('SELECT id FROM artistaMusical')

    names = []
    for url in urls:
        name = re.sub('https://en.wikipedia.org/wiki/', '', url[0])
        name = correctName(name)
        names.append(name)

    return names

def updateDatabase(bands):
    db = DataBaseController('1802LMWTechnologies', '1802LMWTechnologies', '200.134.10.32', '971633')
    for band in bands:
        db.updateTable('artistaMusical', band['id'], band)


def getArtistName(soup):
    artist_name = ""
    spans = []

    infobox = soup.find_all('table', class_='infobox')

    for element in infobox:
        spans = element.find_all('span', class_='fn')
        if spans:
            break

    if len(spans) > 0:
        artist_name = spans[0].text


    return artist_name


def getGenres(soup):

    infobox = None
    tr_tags = None
    td_tags = None
    a_tags = None
    genres = ""
    errors = 0
    keys = ['Genre', 'Genres', 'genre', 'genres']

    try:
        infobox = soup.find_all('table', class_='infobox')

        tr_tags = getElementsInInfoBox(infobox, keys)

        if tr_tags:
            td_tags = getElementsInTrTags(tr_tags, keys)

        if td_tags:
            a_tags = td_tags[0].find_all('a')
            if a_tags:
                for tag in a_tags:
                    genre = tag.text.split('>')
                    if type(genre) is list:
                        genre = genre[0]

                    if checkText(genre):
                        genres += genre + ', '

        if len(genres) > 2:
            genres = genres[0:-2]

    except Exception as e:
        print '\n\n', e, '\n\n'

    if len(genres) > 90:
        list_of_genres = genres.split(',')
        genres = ""
        for i in range(5):
            genres += list_of_genres[i] + ','

        genres = genres[0:-1]

    return genres



def getCityAndCountry(soup):
    city_and_country = ""
    infobox = None
    tr_tags = None
    td_tags = None
    keys = ['Origin', 'Born', 'origin', 'born']

    try:
        infobox = soup.find_all('table', class_='infobox')

        tr_tags = getElementsInInfoBox(infobox, keys)

        if tr_tags:
            td_tags = getElementsInTrTags(tr_tags, keys)

        city_and_country = getCityAndCountryStringFromTdTags(td_tags)

    except Exception as e:
        print '\n\n', e, '\n\n'

    return city_and_country


def getElementsInInfoBox(infobox, keys):
    tr_tags = None

    for element in infobox:
        for key in keys:
            if element.text.find(key) != -1:
                tr_tags = element.find_all('tr')
                break


    return tr_tags



def getElementsInTrTags(tr_tags, keys):
    td_tags = None

    for element in tr_tags:
        for key in keys:
            if element.text.find(key) != -1:
                td_tags = element.find_all('td')
                break

    return td_tags


def getCityAndCountryStringFromTdTags(td_tags):

    span = td_tags[0].find_all('span', class_='birthplace')

    if not span:
        strings = td_tags[0].text.split(',')
        strings[0] = strings[0].replace('\n', '')
        city_and_country = ''
        if len(strings) > 3:
            city_and_country = strings[-3] + ',' + strings[-2] + ',' + strings[-1]
        else:
            for string in strings:
                city_and_country += string + ','
            city_and_country = city_and_country[0:-1]

        if city_and_country.find(')') != -1:
            city_and_country = city_and_country.split(')')[1]

    else:
        city_and_country = span[0].text
        city_and_country = city_and_country.replace('\n', '')

    return city_and_country

def checkText(string):
    result = re.search('[a-z]', string)
    if result:
        return True
        return False


def getBandsInfo(band_names):
    bands = []
    errors = 0
    counter = 0

    band_names = reverseBandNames(band_names)

    last = createLastNetwork()

    for band_name in band_names:
        try:
            url = 'https://en.wikipedia.org/wiki/' + band_name
            headers = {"Accept-Language": "en-US, en;q=0.5"}

            page = requests.get(url, headers)
            soup = BeautifulSoup(page.text, 'lxml')
            artist_name = getArtistName(soup)
            genres = getGenres(soup)
            city_and_country = getCityAndCountry(soup)
            bio = mountBio(last, band_name)

            band = \
            { \
                'id' : url, \
                'nomeartistico' : '', \
                'genero' : '', \
                'pais' : '', \
                'bio': ''
            }

            if artist_name:
                band['nomeartistico'] = artist_name

            if genres:
                band['genero'] = genres

            if city_and_country:
                band['pais'] = city_and_country

            if bio:
                band['bio'] = bio

            print "\n\nBand %d: %s" %(counter, url) + '\n', band, '\n\n'
            bands.append(band)

            counter += 1

        except Exception as e:
            print '\n\n', e, '\n\n', url
            errors += 1

    return bands


def reverseBandNames(band_names):
    names = []
    for band_name in band_names:
        name = band_name.replace(u'á', '%C3%Aa')
        name = name.replace(u'ã', '%C3%A3')
        name = name.replace(u'ç', '%C3%A7')
        name = name.replace(u'é', '%C3%A9')
        name = name.replace(u'ó', '%C3%B3')
        name = name.replace(u'â', '\\xe2')
        name = name.replace(u'ã', '\\xe3')
        name = name.replace(u'ö', '\\xf6')
        name = name.replace(u'\'', '%27')
        name = name.replace('\'', '%27')
        name = name.replace(u'&', '%26')

        names.append(name)

    return names



def writeToXMLFile(bands):
    # print 'xml writing'
    xml_file = open('music.xml', 'w+')
    root = etree.Element('AllBands')
    for band in bands:
        try:
            etree.SubElement(root, 'Band', id=band['id'], nomeartistico=band['nomeartistico'], genero=band['genero'], pais=band['pais'])
        except Exception as e:
            print e
    tree = etree.ElementTree(root)
    tree.write('music.xml', encoding='utf-8', xml_declaration=True, pretty_print=True)



def createLastNetwork():
    API_KEY = "b25b959554ed76058ac220b7b2e0a026"  # this is a sample key
    API_SECRET = "425b55975eed76058ac220b7b4e8a054"

    network = pylast.LastFMNetwork(api_key = API_KEY, api_secret = API_SECRET)

    return network

def mountBio(network, artist_name):
    wrong_entries = ["_(band)", "_(metal_band)", "_(musician)", "_(music_producer)", "_(singer)", "_(rapper)", "_(pop_group)", "_(violinist)"]

    for error in wrong_entries:
	    artist_name = artist_name.replace(error, "")

    artist_name = artist_name.replace("_", " ")

    # print artist_name
    artist = network.get_artist(artist_name)
    bio = artist.get_bio_content(language="pt")

    texto = ""
    for info in bio.split('.'):
        if len(info + texto) < 1000:
            texto = texto + info + "."

        else:
            break

    return texto




def main():
    band_names = getBandNamesFromDatabase()

    bands = getBandsInfo(band_names)

    updateDatabase(bands)

    writeToXMLFile(bands)


if __name__ == "__main__":
    main()
