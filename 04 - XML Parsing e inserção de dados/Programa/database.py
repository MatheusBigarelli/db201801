# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras 

class DataBaseController:
    def __init__(self, dbname, user, host, password):
        self.dbname = dbname
        self.user = user
        self.host = host
        self.password = password

    def connect(self):
        connect_code = "dbname='" + self.dbname + "' user='" + self.user + "' host='" + self.host + "' password='" + self.password +"'"
        try:
            self.conn = psycopg2.connect(connect_code)
            self.cursor = self.conn.cursor()
            print("Connected to the database: " + self.dbname + ".")
        except:
            print("I am unable to connect to the database: " + self.dbname + ".") 
    
    def insertIntoTable(self, table, attributes_list,column_order):
        
        table += "("
        for att in column_order:
            if att == column_order[-1]:
                table += att
            else:
                table += att + ","


        attributes_command = ""
        for attribute in attributes_list:
            if attribute == attributes_list[-1]:
                attributes_command += "'" + attribute + "'"
            else:
             attributes_command += "'" + attribute + "',"
        
        command = "INSERT INTO " + table + ") VALUES(" + attributes_command + ")"
        try:
            print command
            self.cursor.execute(command)
        except:
            pass

        self.conn.commit()

    def countRows(self,table):
        self.cursor.execute("SELECT COUNT(*) FROM " + table)
        return int(self.cursor.fetchall()[0][0])

        
