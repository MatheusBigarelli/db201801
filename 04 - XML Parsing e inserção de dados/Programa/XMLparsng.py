# -*- coding: utf-8 -*-
from xml.dom import minidom
import urllib2
from myparser import Parser
from database import DataBaseController
from pyquery import PyQuery

def getInformationOnInternet():

    info_urls = createInfoUrls()

    parser = Parser()
    parser.setInformationSource(info_urls)
    parser.getInformations()

    return parser

def createInfoUrls():
    info_urls = {}
    info_urls['Person'] = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml'
    info_urls['LikesMusic'] = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml'
    info_urls['LikesMovie'] = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml'
    info_urls['Knows'] = 'http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml'

    return info_urls

def insertDataOnDatabase(table_name, attributes_list, database, column_order):
    for attributes in attributes_list:
        database.insertIntoTable(table_name, attributes,column_order)

def buildQueryForInsertion(minidom_elements):
    pass


parser = Parser()
database = DataBaseController("1802LMWTechnologies", "1802LMWTechnologies", "200.134.10.32", "971633")
database.connect()

parser = getInformationOnInternet()

table_name = 0
order = 0

for table in parser.attributes_list:
    
    if table_name == "CurteArtistaMusical" and isinstance(table, (list, tuple)):
        table_name_to_create = "ArtistaMusical"
        table_to_create = []
        for line in table:
            line_to_create = []
            line_to_create.append(line[1])
            line_to_create.append(line[1].replace("https://en.wikipedia.org/wiki/",""))
            table_to_create.append(line_to_create)

        order = ["ID", "NomeArtistico"]
        insertDataOnDatabase(table_name_to_create, table_to_create, database, order)
    
    if table_name == "CurteFilme" and isinstance(table, (list, tuple)):
        table_name_to_create = "Filme"
        table_to_create = []
        for line in table:
            line_to_create = []
            line_to_create.append(line[1])
            #pg = PyQuery(url=line[1])
            #tag = pg('h1')
            #tag('span').remove()
            #line_to_create.append(tag.text())
            table_to_create.append(line_to_create)

        order = ["ID"]
        insertDataOnDatabase(table_name_to_create, table_to_create, database, order)
    
    if isinstance(table, (list, tuple)):      
            insertDataOnDatabase(table_name, table, database, order)
    
        
    if table == 'Person':
        table_name = "Pessoa"
        order = ["CidadeNatal","Login","DataNascimento","Nome"]
    if table == "LikesMusic":
        table_name = "CurteArtistaMusical"
        order = ["Login_Pessoa","ID_ArtistaMusical","Nota"]
    if table == "LikesMovie":
        table_name = "CurteFilme"
        order = ["Login_Pessoa","ID_Filme","Nota"]
    if table == "Knows":
        table_name = "Conhece"
        order = ["Login1","Login2"]

