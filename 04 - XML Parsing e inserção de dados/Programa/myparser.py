from xml.dom import minidom
import urllib2

class Parser:
    TAG = 0
    URL = 1

    def __init__(self):
        pass

    def setInformationSource(self, info_urls):
        self.info_urls = info_urls

    def getInformations(self):
        xmlDocuments = self.fetchXmlDocumentsInSources()

        universes = {}
        self.elements = {}
        for tag in self.info_urls:
            universes[tag] = xmlDocuments[tag].documentElement
            self.elements[tag] = universes[tag].getElementsByTagName(tag)

        self.attributes_list = []
        for tables in self.elements.values():
            all_attributes = []
            for record in tables:
                attributes =[]
                for attribute in record.attributes.keys():
                    try:
                        aux = record.attributes[attribute].value
                        if 'http://utfpr.edu.br/CSB30/2018/2/' in aux:
                            aux = aux.replace("http://utfpr.edu.br/CSB30/2018/2/","")  
                        elif 'http://www.imdb.com/title/' in aux:
                            aux = aux.replace("http://www.imdb.com/title/","")
                            aux = aux.replace("/","")
                        attributes.append(aux)
                    except:
                        pass
                all_attributes.append(attributes)
                
            self.attributes_list.append(record.tagName)
            self.attributes_list.append(all_attributes)

    def fetchXmlDocumentsInSources(self):
        docs = {}
        for tag in self.info_urls:
            docs[tag] = minidom.parse(urllib2.urlopen(self.info_urls[tag]))

        return docs

 
