from xml.dom.minidom import parse
import xml.dom.minidom
import csv

#Biblioteca usada somente para criar o diretorio dadosMarvel diretamente
import os
os.system("mkdir -p dadosMarvel")

#Arquivo onde serao guardados os dados dos herois da marvel.
herois = open("dadosMarvel/herois.csv", "w")
herois_good = open("dadosMarvel/herois_good.csv", "w")
herois_bad = open("dadosMarvel/herois_bad.csv", "w")

#Variaveis para dados pedidos
goods = 0
bads = 0
pesos = 0
total_herois = 0

# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse('marvel_simplificado.xml')

universe = DOMTree.documentElement

# Get all the heroes in the universe
heroes = universe.getElementsByTagName('hero')

for hero in heroes:
    dados = [] #Lista de dados que usaremos para gravar no arquivo

    if hero.hasAttribute('id'):
        id = hero.getAttribute('id')
        dados.append(id)

    name = hero.getElementsByTagName('name')[0]
    dados.append(name.childNodes[0].data)

    popularity = hero.getElementsByTagName('popularity')[0]
    dados.append(popularity.childNodes[0].data)

    alignment = hero.getElementsByTagName('alignment')[0]
    dados.append(alignment.childNodes[0].data)

    gender = hero.getElementsByTagName('gender')[0]
    dados.append(gender.childNodes[0].data)

    height_m = hero.getElementsByTagName('height_m')[0]
    dados.append(height_m.childNodes[0].data)

    weight_kg = hero.getElementsByTagName('weight_kg')[0]
    dados.append(weight_kg.childNodes[0].data)

    hometown = hero.getElementsByTagName('hometown')[0]
    dados.append(hometown.childNodes[0].data)

    intelligence = hero.getElementsByTagName('intelligence')[0]
    dados.append(intelligence.childNodes[0].data)

    strength = hero.getElementsByTagName('strength')[0]
    dados.append(strength.childNodes[0].data)

    speed = hero.getElementsByTagName('speed')[0]
    dados.append(speed.childNodes[0].data)

    durability = hero.getElementsByTagName('durability')[0]
    dados.append(durability.childNodes[0].data)

    energy_Projection = hero.getElementsByTagName('energy_Projection')[0]
    dados.append(energy_Projection.childNodes[0].data)

    fighting_Skills = hero.getElementsByTagName('fighting_Skills')[0]
    dados.append(fighting_Skills.childNodes[0].data)

    writer = csv.writer(herois)
    writer.writerow(dados)

    if alignment.childNodes[0].data == 'Good':
        writer = csv.writer(herois_good)
        writer.writerow(dados)
        goods += 1
    if alignment.childNodes[0].data == 'Bad':
        writer = csv.writer(herois_bad)
        writer.writerow(dados)
        bads += 1

    #Para calcular a media do peso dos personagens
    pesos += float(weight_kg.childNodes[0].data)
    total_herois += 1

    #Canculando o indice de massa corporal do hulk
    if name.childNodes[0].data == 'Hulk':
        peso_Hulk = float(weight_kg.childNodes[0].data)
        altura_Hulk = float(height_m.childNodes[0].data)
        IMC =  peso_Hulk/(altura_Hulk*altura_Hulk)

herois.close
herois_bad.close
herois_good.close

proportion = float(goods)/bads

print ('Proporcao entre herois bons e maus (bons/maus): ' + str(proportion))
print ('Media de peso dos herois: ' + str(round(pesos/total_herois, 2)))
print ('Indice de massa corporal (IMC) do Hulk: ' + str(IMC))
